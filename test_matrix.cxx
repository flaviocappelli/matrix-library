/*
** Copyright (C) 2018 Flavio Cappelli
** See Copyright Notice in matrix.hxx
*/

// Uncomment this define or build with -DNOBLASLAPACK to
// disable any dependency from BLAS and LAPACK libraries.
//#define NOBLASLAPACK

#include "matrix.hxx"
#include <iostream>

using namespace std;


void test_matrix_types();
void test_matrix_constructors();
void test_matrix_copy_ctor_assign();
void test_matrix_move_ctor_assign();
void test_access_to_elements();
void test_static_members();
void test_conj_tran_ctran_imag_real_submat_resize();
void test_unary_operators_plus_minus();
void test_addition();
void test_subtraction();
void test_arrymul_and_product_byel();
void test_arrydiv_and_divide_byel();
void test_matrix_product();
void check_lu_y(const Matrix_D &, const Matrix_LD &);
void check_lu_lu(const Matrix_D &, const Matrix_LD &);
void check_lu_lup(const Matrix_D &, const Matrix_LD &);
void test_lu_decomposition(void (* check_lu_decomp)(const Matrix_D &, const Matrix_LD &));
void test_left_division();
void test_right_division();
void test_matrix_inversion();
void test_determinant();


int main()
{
    int test_n;
    cout << "\n\nHello Matrix!";

    do {
        cout << "\n\n-----\n\n";
        try {
            cout << "Available tests: \n"
                    "  0 - quit\n"
                    "  1 - test matrix types\n"
                    "  2 - test matrix constructors\n"
                    "  3 - test matrix copy constructor & assignment (implicit sharing)\n"
                    "  4 - test matrix move constructor & assignment\n"
                    "  5 - test matrix access to single elements\n"
                    "  6 - test static members eye, zeros, ones, rand, randn, hilb\n"
                    "  7 - test functions conj, tran, ctran, imag, real, submat, resize\n"
                    "  8 - test unary operators '+' and '-'\n"
                    "  9 - test binary operator '+' (addition)\n"
                    " 10 - test binary operator '-' (subtraction)\n"
                    " 11 - test array_mul and operator '*' on matrix elements\n"
                    " 12 - test array_div and operator '/' on matrix elements\n"
                    " 13 - test matrix multiplication '*' (rows by columns)\n"
                    " 14 - test LU decomposition (out Y)\n"
                    " 15 - test LU decomposition (out L,U)\n"
                    " 16 - test LU decomposition (out L,U,P)\n"
                    " 17 - test mldivide and linsolve (left division)\n"
                    " 18 - test mrdivide and operator '/' (right division)\n"
                    " 19 - test matrix inversion\n"
                    " 20 - test determinant\n"
                    "\n"
                    "Test number ? ";
            cin >> test_n;
            cout << endl;

            switch (test_n)
            {
            case 0:
                break;
            case 1:
                test_matrix_types();
                break;
            case 2:
                test_matrix_constructors();
                break;
            case 3:
                test_matrix_copy_ctor_assign();
                break;
            case 4:
                test_matrix_move_ctor_assign();
                break;
            case 5:
                test_access_to_elements();
                break;
            case 6:
                test_static_members();
                break;
            case 7:
                test_conj_tran_ctran_imag_real_submat_resize();
                break;
            case 8:
                test_unary_operators_plus_minus();
                break;
            case 9:
                test_addition();
                break;
            case 10:
                test_subtraction();
                break;
            case 11:
                test_arrymul_and_product_byel();
                break;
            case 12:
                test_arrydiv_and_divide_byel();
                break;
            case 13:
                test_matrix_product();
                break;
            case 14:
                test_lu_decomposition(check_lu_y);
                break;
            case 15:
                test_lu_decomposition(check_lu_lu);
                break;
            case 16:
                test_lu_decomposition(check_lu_lup);
                break;
            case 17:
                test_left_division();
                break;
            case 18:
                test_right_division();
                break;
            case 19:
                test_matrix_inversion();
                break;
            case 20:
                test_determinant();
                break;
            }
        }

        catch (const exception & e)
        {
            cerr << endl << "caught exception: " << e.what() << endl << endl;
        }

        catch (...) {
            cerr << endl << "unknow exception" << endl << endl;
        }

    } while (test_n != 0);
    return 0;
}


void test_matrix_types()
{
    cout << endl << endl << endl;
    cout << "TEST ALLOCATION OF MATRICES (DIFFERENT TYPES)" << endl << endl;

    cout << "TYPE: INT" << endl;
    Matrix_I M1;
    MATRIX_DUMP_INTERNAL(M1,false);
    cout << endl;

    cout << "TYPE: FLOAT" << endl;
    Matrix_F M2;
    MATRIX_DUMP_INTERNAL(M2,false);
    cout << endl;

    cout << "TYPE: DOUBLE" << endl;
    Matrix_D M3;
    MATRIX_DUMP_INTERNAL(M3,false);
    cout << endl;

    cout << "TYPE: LONG DOUBLE" << endl;
    Matrix_LD M4;
    MATRIX_DUMP_INTERNAL(M4,false);
    cout << endl;

    cout << "TYPE: COMPLEX INT" << endl;
    Matrix_CI M5;
    MATRIX_DUMP_INTERNAL(M5,false);
    cout << endl;

    cout << "TYPE: COMPLEX FLOAT" << endl;
    Matrix_CF M6;
    MATRIX_DUMP_INTERNAL(M6,false);
    cout << endl;

    cout << "TYPE: COMPLEX DOUBLE" << endl;
    Matrix_CD M7;
    MATRIX_DUMP_INTERNAL(M7,false);
    cout << endl;

    cout << "TYPE: COMPLEX LONG DOUBLE" << endl;
    Matrix_CLD M8;
    MATRIX_DUMP_INTERNAL(M8,false);
    cout << endl;

    cout << "TYPE: CUSTOM LONG LONG" << endl;
    MatrixBase<long long> M9;
    MATRIX_DUMP_INTERNAL(M9,false);
    cout << endl;
}


void test_matrix_constructors()
{
    cout << endl << endl << endl;
    cout << "TEST ALLOCATION OF MATRICES (DIFFERENT CONSTRUCTORS)" << endl << endl;

    Matrix_F M1;
    MATRIX_PRINT(M1);
    cout << endl;
    MATRIX_DUMP_INTERNAL(M1,true);
    cout << endl << endl;

    Matrix_F M2(3,3);
    MATRIX_PRINT(M2);
    cout << endl;
    MATRIX_DUMP_INTERNAL(M2,true);
    cout << endl << endl;

    float array[6] = {1,2,3,
                      4,5,6};
    Matrix_F M3(2,3, array);
    MATRIX_PRINT(M3);
    cout << endl;
    MATRIX_DUMP_INTERNAL(M3,true);
    cout << endl << endl;

    Matrix_F M4(3,4, {1, 2, 3, 4,
                      5, 6, 7, 8,
                      9,10,11,12});
    MATRIX_PRINT(M4);
    cout << endl;
    MATRIX_DUMP_INTERNAL(M4,true);
    cout << endl << endl;

    Matrix_F M5 = {
        {-1, 2,-3, 4},
        { 5,-6, 7,-8}
    };
    MATRIX_PRINT(M5);
    cout << endl;
    MATRIX_DUMP_INTERNAL(M5,true);
    cout << endl << endl;

    Matrix_F M6(100);
    MATRIX_PRINT(M6);
    cout << endl;
    MATRIX_DUMP_INTERNAL(M6,true);
    cout << endl << endl;

    Matrix_CI M7 = {
        {cplxi(-1,  2),  cplxi(0,7)},
        {cplxi( 3,-42),  cplxi(2,3)}
    };
    MATRIX_PRINT(M7);
    cout << endl;
    MATRIX_DUMP_INTERNAL(M7,true);
    cout << endl << endl;

    cout << "As you can see, elements are internally stored with the COLUMN MAJOR order\n"
            "(storage required to be compatible with the BLAS/LAPACK Fortran interface).\n\n";
}


void test_matrix_copy_ctor_assign()
{
    cout << endl << endl << endl;
    cout << "TEST COPY CONSTRUCTORS/ASSIGNMENT (IMPLICIT SHARING)" << endl << endl;

    cout << "Define Matrix A (not null), then define Matrix B=A" << endl << endl;
    Matrix_D A = {{1,2,3,4},{5,6,7,8}};
    Matrix_D B = A;

    MATRIX_DUMP_INTERNAL(A,true);
    cout << endl;
    MATRIX_DUMP_INTERNAL(B,true);
    cout << endl;

    cout << "As you can see (from the m_d pointer and the reference counter) the container is shared!" << endl << endl << endl;

    do {
        cout << "Inside a new block (scope) define Matrix C, then assign C=A" << endl << endl;

        Matrix_D C;
        C = A;

        MATRIX_DUMP_INTERNAL(C,true);
        cout << endl;

        cout << "As you can see A,B,C point to the same container" << endl << endl << endl;

        cout << "Now define Matrix D, then assign B=D, then dump all matrices" << endl << endl;

        Matrix_D D = {{9,8,7,6},{5,4,3,2}};
        B = D;

        MATRIX_DUMP_INTERNAL(A,true);
        cout << endl;
        MATRIX_DUMP_INTERNAL(B,true);
        cout << endl;
        MATRIX_DUMP_INTERNAL(C,true);
        cout << endl;
        MATRIX_DUMP_INTERNAL(D,true);
        cout << endl;

        cout << "As you can see now A and C are shared, and B and D are shared" << endl << endl << endl;

    } while(0);

    cout << "Now exit from the inside block, C and D are destroyed" << endl << endl;

    MATRIX_DUMP_INTERNAL(A,true);
    cout << endl;
    MATRIX_DUMP_INTERNAL(B,true);
    cout << endl;

    cout << "As you can see the reference counter of A and B has been decreased" << endl << endl << endl;

}


void test_matrix_move_ctor_assign()
{
    cout << endl << endl << endl;
    cout << "TEST MOVE CONSTRUCTORS/ASSIGNMENT" << endl << endl;

    cout << "Define Matrix A (not null), then define Matrix B = move(A)" << endl << endl;

    Matrix_D A = {{1,2,3,4},{5,6,7,8}};
    Matrix_D B = move(A);

    MATRIX_DUMP_INTERNAL(A,true);
    cout << endl;
    MATRIX_DUMP_INTERNAL(B,true);
    cout << endl;

    cout << "As you can see (from the m_d pointer) the A container has been moved to B" << endl << endl << endl;

    cout << "Now define Matrix C, then assign C = move(B), then dump all" << endl << endl;

    Matrix_D C;
    C = move(B);

    MATRIX_DUMP_INTERNAL(A,true);
    cout << endl;
    MATRIX_DUMP_INTERNAL(B,true);
    cout << endl;
    MATRIX_DUMP_INTERNAL(C,true);
    cout << endl;

    cout << "As you can see, the B container has been moved to C" << endl << endl << endl;
}


void test_access_to_elements()
{
    cout << endl << endl << endl;
    cout << "TEST ACCESS TO MATRIX ELEMENTS" << endl << endl;

    cout << "Define a 4x4 double identity matrix" << endl << endl;

    Matrix_D M = Matrix_D::eye(4);
    MATRIX_PRINT(M);
    cout << endl << endl;

    cout << "Now print some element (indexes start from zero):\n";
    cout << "M(0,0) = " << M(0,0) << endl;
    cout << "M(1,2) = " << M(1,2) << endl;
    cout << endl << endl;

    cout << "Now assign an element:\n";
    cout << "M(1,1) = 10\n\n";

    M(1,1) = 10;
    MATRIX_PRINT(M);
    cout << endl << endl;

    cout << "Now assign another element with indexes out of range, an exception is raised\n";

    M(5,5) = 7;
}


void test_static_members()
{
    cout << endl << endl << endl;
    cout << "TEST MATRIX STATIC MEMBERS" << endl << endl;

    cout << "Identity" << endl;
    MATRIX_PRINT(Matrix_D::eye(4));
    cout << endl << endl;

    cout << "All ones" << endl;
    MATRIX_PRINT(Matrix_D::ones(3,5));
    cout << endl << endl;

    cout << "All zeros" << endl;
    MATRIX_PRINT(Matrix_D::zeros(3,5));
    cout << endl << endl;

    cout << "Uniformly distributed random elements (range [0,1])" << endl;
    MATRIX_PRINT(Matrix_D::rand(3,5));
    cout << endl << endl;

    cout << "Uniformly distributed random elements (range [-10,10])" << endl;
    MATRIX_PRINT(Matrix_D::rand(3,5, -10.0, 10.0));
    cout << endl << endl;

    cout << "Normally distributed random elements (mean 0.0, standard deviation 1.0)" << endl;
    MATRIX_PRINT(Matrix_D::randn(3,5));
    cout << endl << endl;

    cout << "Normally distributed random elements (mean 0.0, standard deviation 1000.0)" << endl;
    MATRIX_PRINT(Matrix_D::randn(3,5, 0.0, 1000.0));
    cout << endl << endl;

    cout << "Hilbert matrix of 4th order" << endl;
    MATRIX_PRINT(Matrix_D::hilb(4));
    cout << endl << endl;
}


void test_conj_tran_ctran_imag_real_submat_resize()
{
    cout << endl << endl << endl;
    cout << "TEST OTHER MATRIX MEMBERS" << endl << endl;

    Matrix_CI A = {
        {cplxi(-1,  2), cplxi(0,7), cplxi(21, -2), cplxi(0, 6)},
        {cplxi( 3,-42), cplxi(2,3), cplxi(-10, 0), cplxi(11,2)},
        {cplxi( 1,  7), cplxi(0,0), cplxi(15, 70), cplxi(-1,9)},
    };
    MATRIX_PRINT(A);
    cout << endl << endl;

    cout << "Complex conjugate" << endl;
    MATRIX_PRINT(A.conj());
    cout << endl << endl;

    cout << "Transpose" << endl;
    MATRIX_PRINT(A.tran());
    cout << endl << endl;

    cout << "Complex conjugate transpose" << endl;
    MATRIX_PRINT(A.ctran());
    cout << endl << endl;

    cout << "Real part" << endl;
    MATRIX_PRINT(A.real());
    cout << endl << endl;

    cout << "Immaginary part" << endl;
    MATRIX_PRINT(A.imag());
    cout << endl << endl;

    cout << "Submatrix (indexes start from zero)" << endl;
    MATRIX_PRINT(A.submat(1,1,2,2));
    cout << endl << endl;

    cout << "Resize" << endl;
    A.resize(5,5);
    MATRIX_PRINT(A);
    cout << endl << endl;
}


void test_unary_operators_plus_minus()
{
    cout << endl << endl << endl;
    cout << "TEST UNARY OPERATORS + AND -" << endl << endl;

    Matrix_D M = {
        {1, 0, 1, -1},
        {0, 2, -2, 0},
        {1, 1,  2, 0},
        {2, 0,  2, 0}
    };
    MATRIX_PRINT(M);
    cout << endl;
    MATRIX_DUMP_INTERNAL(M,true);
    cout << endl << endl;

    cout << "M1 = +M" << endl;
    Matrix_D M1 = +M;
    MATRIX_PRINT(M1);
    cout << endl;
    MATRIX_DUMP_INTERNAL(M1,true);
    cout << endl << endl;

    cout << "M2 = -M" << endl;
    Matrix_D M2 = -M;
    MATRIX_PRINT(M2);
    cout << endl;
    MATRIX_DUMP_INTERNAL(M2,true);
    cout << endl << endl;
}


void test_addition()
{
    cout << endl << endl << endl;
    cout << "TEST BINARY OPERATOR + (ADDITION)" << endl << endl;

    Matrix_I M1 = {
        {1, 0, 1, -1},
        {0, 2, -2, 0},
        {1, 1,  2, 0}
    };
    Matrix_I M2 = {
        {0,  0,  1,  0},
        {7,  0,  2,  0},
        {1,  0,  0,  2}
    };
    MATRIX_PRINT(M1);
    cout << endl;
    MATRIX_PRINT(M2);
    cout << endl;
    MATRIX_PRINT(5+M1);
    cout << endl;
    MATRIX_PRINT(M1+5);
    cout << endl;
    MATRIX_PRINT(M1+M2);
    cout << endl << endl;

    Matrix_CI M3 = {
        {cplxi(-1,  2), cplxi(0,7), cplxi(21, -2), cplxi(0, 6)},
        {cplxi( 3,-42), cplxi(2,3), cplxi(-10, 0), cplxi(11,2)},
        {cplxi( 1,  7), cplxi(0,0), cplxi(15, 70), cplxi(-1,9)}
    };
    MATRIX_PRINT(M3);
    cout << endl;
    MATRIX_PRINT(M1+M3);
    cout << endl << endl;

    cout << "Add two matrices with different dimension: an exception is raised.\n\n";
    Matrix_D A = Matrix_D::eye(5);
    Matrix_D B = Matrix_D::ones(3,4);
    Matrix_D C = A+B;
}


void test_subtraction()
{
    cout << endl << endl << endl;
    cout << "TEST BINARY OPERATOR - (SUBTRACTION)" << endl << endl;

    Matrix_I M1 = {
        {1, 0, 1, -1},
        {0, 2, -2, 0},
        {1, 1,  2, 0}
    };
    Matrix_I M2 = {
        {0,  0,  1,  0},
        {7,  0,  2,  0},
        {1,  0,  0,  2}
    };
    MATRIX_PRINT(M1);
    cout << endl;
    MATRIX_PRINT(M2);
    cout << endl;
    MATRIX_PRINT(M1-10);
    cout << endl;
    MATRIX_PRINT(10-M1);
    cout << endl;
    MATRIX_PRINT(M1-M2);
    cout << endl << endl;

    Matrix_CI M3 = {
        {cplxi(-1,  2), cplxi(0,7), cplxi(21, -2), cplxi(0, 6)},
        {cplxi( 3,-42), cplxi(2,3), cplxi(-10, 0), cplxi(11,2)},
        {cplxi( 1,  7), cplxi(0,0), cplxi(15, 70), cplxi(-1,9)}
    };
    MATRIX_PRINT(M3);
    cout << endl;
    MATRIX_PRINT(M1-M3);
    cout << endl << endl;

    cout << "Subtract two matrices with different dimension: an exception is raised.\n\n";
    Matrix_D A = Matrix_D::eye(5);
    Matrix_D B = Matrix_D::ones(3,4);
    Matrix_D C = A-B;
}


void test_arrymul_and_product_byel()
{
    Matrix_D M1 = {
        {1, 2,  1},
        {0, 2,  1},
        {1, 1,  2}
    };
    Matrix_D M2 = {
        {1.0, 3.0, 5.0},
        {2.0, 4.5, 7.0},
        {1.0, 1.0, 0.0}
    };
    MATRIX_PRINT(M1);
    cout << endl;
    MATRIX_PRINT(M2);
    cout << endl;
    MATRIX_PRINT(array_mul(M1,10));
    cout << endl;
    MATRIX_PRINT(array_mul(10,M1));
    cout << endl;
    MATRIX_PRINT(array_mul(M1,M2));
    cout << endl << endl;

    Matrix_CD M3 = {
        {cplxd(0,7), cplxd( 21,-2), cplxd(0, 6)},
        {cplxd(2,3), cplxd(-10, 0), cplxd(11,2)},
        {cplxd(1,7), cplxd(  0, 0), cplxd(-1,9)}
    };
    MATRIX_PRINT(M3);
    cout << endl;
    MATRIX_PRINT(array_mul(M1,M3));
    cout << endl << endl;
}


void test_arrydiv_and_divide_byel()
{
    Matrix_D M1 = {
        {1, 2,  1},
        {0, 2,  0},
        {1, 1,  2}
    };
    Matrix_D M2 = {
        {1.0, 3.0, 5.0},
        {2.0, 4.5, 7.0},
        {1.0, 1.0, 1.0}
    };
    MATRIX_PRINT(M1);
    cout << endl;
    MATRIX_PRINT(M2);
    cout << endl;
    MATRIX_PRINT(array_div(M1,10));
    cout << endl;
    MATRIX_PRINT(array_div(10,M1));
    cout << endl;
    MATRIX_PRINT(array_div(M1,M2));
    cout << endl << endl;

    Matrix_CD M3 = {
        {cplxd(0,7), cplxd( 21,-2), cplxd(0, 6)},
        {cplxd(2,3), cplxd(-10, 0), cplxd(11,2)},
        {cplxd(1,7), cplxd(  0, 1), cplxd(-1,9)}
    };
    MATRIX_PRINT(M3);
    cout << endl;
    MATRIX_PRINT(array_div(M1,M3));
    cout << endl << endl;
}


void test_matrix_product()
{
    cout << endl << endl << endl;
    cout << "TEST MATRIX PRODUCT" << endl << endl;


    Matrix_D A = {
        {10.5}
    };
    MATRIX_PRINT(A);
    cout << endl ;
    MATRIX_PRINT(A*A);
    cout << endl << endl;


    Matrix_D B = {
        {-7,  7, 10, 8},
        { 8,  2,  3, 3},
        { 3, -1,  6, 7},
        { 7,  5, -2, 5},
    };
    MATRIX_PRINT(B);
    cout << endl ;
    MATRIX_PRINT(B*B);
    cout << endl << endl;


    Matrix_D C {
        {1, 2, 3},
        {4, 5, 6}
    };
    Matrix_D D {
        { 1, -2},
        { 0,  5},
        {-1,  2}
    };
    MATRIX_PRINT(C);
    cout << endl ;
    MATRIX_PRINT(D);
    cout << endl ;
    MATRIX_PRINT(C*D);
    cout << endl << endl;

    MATRIX_PRINT(10*C);
    cout << endl << endl;

    MATRIX_PRINT(D*10);
    cout << endl << endl;


    Matrix_CD E = {
        { cplxd(11,-11), cplxd(12,-12), cplxd(13,-13), cplxd(14,-14) },
        { cplxd(21,-21), cplxd(22,-22), cplxd(23,-23), cplxd(24,-24) }
    };
    MATRIX_PRINT(E);
    cout << endl ;
    MATRIX_PRINT(cplxd(0.5,0.5)*E);
    cout << endl << endl;


    MATRIX_PRINT(D*E);
    cout << endl << endl;


    RowVec_D V1 = RowVec_D::rand(4);
    ColVec_D V2 = ColVec_D::rand(4);
    MATRIX_PRINT(V1);
    cout << endl ;
    MATRIX_PRINT(V2);
    cout << endl ;
    MATRIX_PRINT(V1*V2);
    cout << endl ;
    MATRIX_PRINT(V2*V1);
    cout << endl << endl;


    cout << "Multiply two matrices with wrong sizes: an exception is raised.\n\n";
    Matrix_D X = Matrix_D::ones(3,4);
    Matrix_D Y = Matrix_D::ones(3,4);
    MATRIX_PRINT(X);
    cout << endl;
    MATRIX_PRINT(Y);
    cout << endl;
    MATRIX_PRINT(X*Y);
}


void check_lu_y(const Matrix_D & M1, const Matrix_LD & M2)
{
    // Safety check.
    Matrix_D M2d = static_cast<Matrix_D>(M2);
    assert(almost_equal(M1,M2d));


    Matrix_D  Y1;
    Matrix_LD Y2;
    bool error = false;

    cout << "DOUBLE Matrix (LU decomposition use LAPACK DGETRS)" << endl << endl;

    LU_dec(M1,Y1);
    MATRIX_PRINT(M1);
    cout << endl;
    MATRIX_PRINT(Y1);
    cout << endl;

    cout << "LONG DOUBLE Matrix (LU decomposition use my GETRS)" << endl << endl;

    LU_dec(M2,Y2);
    MATRIX_PRINT(M2);
    cout << endl;
    MATRIX_PRINT(Y2);
    cout << endl;

    // Cast Y2 long double matrix to double matrix and compare with Y1.
    Matrix_D Y2d = static_cast<Matrix_D>(Y2);
    if (!almost_equal(Y1, Y2d)) {
        cout << "WARNING: Y1 != Y2" << endl << endl << endl;
        error = true;
    }

    if (!error)
        cout << "Check ok" << endl << endl << endl;
}


void check_lu_lu(const Matrix_D & M1, const Matrix_LD & M2)
{
    // Safety check.
    Matrix_D M2d = static_cast<Matrix_D>(M2);
    assert(almost_equal(M1,M2d));

    Matrix_D  L1, U1;
    Matrix_LD L2, U2;
    bool error = false;

    cout << "DOUBLE Matrix (LU decomposition use LAPACK DGETRS)" << endl << endl;

    LU_dec(M1,L1,U1);
    MATRIX_PRINT(M1);
    cout << endl;
    MATRIX_PRINT(L1);
    cout << endl;
    MATRIX_PRINT(U1);
    cout << endl;

    cout << "LONG DOUBLE Matrix (LU decomposition use my GETRS)" << endl << endl;

    LU_dec(M2,L2,U2);
    MATRIX_PRINT(M2);
    cout << endl;
    MATRIX_PRINT(L2);
    cout << endl;
    MATRIX_PRINT(U2);
    cout << endl;

    // Cast L2 long double matrix to double matrix and compare with L1.
    Matrix_D L2d = static_cast<Matrix_D>(L2);
    if (!almost_equal(L1, L2d)) {
        cout << "WARNING: L1 != L2" << endl << endl << endl;
        error = true;
    }

    // Cast U2 long double matrix to double matrix and compare with U1.
    Matrix_D U2d = static_cast<Matrix_D>(U2);
    if (!almost_equal(U1, U2d)) {
        cout << "WARNING: U1 != U2" << endl << endl << endl;
        error = true;
    }

    // Check if M1 = L1*U1.
    if (!almost_equal(M1, L1*U1)) {
        cout << "WARNING: M1 != L1*U1" << endl << endl << endl;
        error = true;
    }

    // Check if M2 = L2*U2.
    if (!almost_equal(M2, L2*U2)) {
        cout << "WARNING: M2 != L2*U2" << endl << endl << endl;
        error = true;
    }

    if (!error)
        cout << "Check ok" << endl << endl << endl;
}


void check_lu_lup(const Matrix_D & M1, const Matrix_LD & M2)
{
    // Safety check.
    Matrix_D M2d = static_cast<Matrix_D>(M2);
    assert(almost_equal(M1,M2d));

    Matrix_I  P1, P2;
    Matrix_D  L1, U1;
    Matrix_LD L2, U2;
    bool error = false;

    cout << "DOUBLE Matrix (LU decomposition use LAPACK DGETRS)" << endl << endl;

    LU_dec(M1,L1,U1,P1);
    MATRIX_PRINT(M1);
    cout << endl;
    MATRIX_PRINT(L1);
    cout << endl;
    MATRIX_PRINT(U1);
    cout << endl;
    MATRIX_PRINT(P1);
    cout << endl;

    cout << "LONG DOUBLE Matrix (LU decomposition use my GETRS)" << endl << endl;

    LU_dec(M2,L2,U2,P2);
    MATRIX_PRINT(M2);
    cout << endl;
    MATRIX_PRINT(L2);
    cout << endl;
    MATRIX_PRINT(U2);
    cout << endl;
    MATRIX_PRINT(P2);
    cout << endl;

    // Cast L2 long double matrix to double matrix and compare with L1.
    Matrix_D L2d = static_cast<Matrix_D>(L2);
    if (!almost_equal(L1, L2d)) {
        cout << "WARNING: L1 != L2" << endl << endl << endl;
        error = true;
    }

    // Cast U2 long double matrix to double matrix and compare with U1.
    Matrix_D U2d = static_cast<Matrix_D>(U2);
    if (!almost_equal(U1, U2d)) {
        cout << "WARNING: U1 != U2" << endl << endl << endl;
        error = true;
    }

    // Compare integer permutation matrices.
    if (P1 != P2) {
        cout << "WARNING: P1 != P2" << endl << endl << endl;
        error = true;
    }

    // Check if P1*M1 = L1*U1.
    if (!almost_equal(P1*M1, L1*U1)) {
        cout << "WARNING: P1*M1 != L1*U1" << endl << endl << endl;
        error = true;
    }

    // Check if P2*M2 = L2*U2.
    if (!almost_equal(P2*M2, L2*U2)) {
        cout << "WARNING: P2*M2 != L2*U2" << endl << endl << endl;
        error = true;
    }

    if (!error)
        cout << "Check ok" << endl << endl << endl;
}


void test_lu_decomposition(void (* check_lu_decomp)(const Matrix_D &, const Matrix_LD &))
{
    assert(check_lu_decomp != NULL);

    cout << endl << endl << endl;
    cout << "TEST LU DECOMPOSITION" << endl << endl;

    Matrix_D A1 = {
        {1, 0,  1, -1},
        {0, 2, -2,  0},
        {1, 1,  2,  0},
        {2, 0,  2,  0}
    };
    Matrix_LD A2 = {
        {1, 0,  1, -1},
        {0, 2, -2,  0},
        {1, 1,  2,  0},
        {2, 0,  2,  0}
    };
    check_lu_decomp(A1,A2);

    Matrix_D B1 = {
        {1, 2,  1, 0},
        {0, 2, -2, 0},
        {1, 1,  2, 0},
        {2, 0,  2, 0}
    };
    Matrix_LD B2 = {
        {1, 2,  1, 0},
        {0, 2, -2, 0},
        {1, 1,  2, 0},
        {2, 0,  2, 0}
    };
    check_lu_decomp(B1,B2);

    Matrix_D C1 = {
        {1.0, 3.0, 5.0},
        {2.0, 4.5, 7.0},
        {1.0, 1.0, 0.0}
    };
    Matrix_LD C2 = {
        {1.0, 3.0, 5.0},
        {2.0, 4.5, 7.0},
        {1.0, 1.0, 0.0}
    };
    check_lu_decomp(C1,C2);

    Matrix_D D1 = {
        {11.0,  9.0, 24.0, 2.0},
        { 1.5,  5.0,  2.0, 6.0},
        { 3.0, 17.0, 18.0, 1.5},
        { 2.0,  5.0,  7.0, 1.0}
    };
    Matrix_LD D2 = {
        {11.0,  9.0, 24.0, 2.0},
        { 1.5,  5.0,  2.0, 6.0},
        { 3.0, 17.0, 18.0, 1.5},
        { 2.0,  5.0,  7.0, 1.0}
    };
    check_lu_decomp(D1,D2);

    Matrix_D E1 = {
        {8,  0,  4,  0,  0,  7},
        {0,  5,  0,  0,  0,  0},
        {3,  0,  0,  3,  0,  0},
        {0,  0,  2, -1,  0,  0},
        {0,  0,  0,  0,  4,  0},
        {0,  7,  0,  0,  0,  3}
    };
    Matrix_LD E2 = {
        {8,  0,  4,  0,  0,  7},
        {0,  5,  0,  0,  0,  0},
        {3,  0,  0,  3,  0,  0},
        {0,  0,  2, -1,  0,  0},
        {0,  0,  0,  0,  4,  0},
        {0,  7,  0,  0,  0,  3}
    };
    check_lu_decomp(E1,E2);

    Matrix_D F1 = {
        {1, 0,  1, -1, 0},
        {0, 2, -2,  0, 3},
        {1, 1,  2,  0, 0}
    };
    Matrix_LD F2 = {
        {1, 0,  1, -1, 0},
        {0, 2, -2,  0, 3},
        {1, 1,  2,  0, 0}
    };
    check_lu_decomp(F1,F2);

    Matrix_D G1 = {
        {8,  0,  4,  0},
        {0,  5,  0,  0},
        {3,  0,  3,  0},
        {0,  2, -1,  0},
        {0,  0,  0,  4},
        {7,  0,  0,  3}
    };
    Matrix_LD G2 = {
        {8,  0,  4,  0},
        {0,  5,  0,  0},
        {3,  0,  3,  0},
        {0,  2, -1,  0},
        {0,  0,  0,  4},
        {7,  0,  0,  3}
    };
    check_lu_decomp(G1,G2);
}


void test_left_division()
{
    cout << endl << endl << endl;
    cout << "TEST MATRIX LEFT DIVISION (mldivide, linsolve)" << endl << endl;

    cout << "A DOUBLE, B DOUBLE" << endl << endl;
    Matrix_D A1 = {
        {-7,  7, 10, 8},
        { 8,  2,  3, 3},
        { 3, -1,  6, 7},
        { 7,  5, -2, 5},
    };
    Matrix_D B1 = {
        {1, 0},
        {0, 2},
        {1, 1},
        {2, 0}
    };
    MATRIX_PRINT(A1);
    cout << endl;
    MATRIX_PRINT(B1);
    cout << endl;
    MATRIX_PRINT(mldivide(A1,B1));
    cout << endl;

    auto X1 = mldivide(A1,B1);
    if (!almost_equal(A1*X1, B1)) {
        cout << "WARNING: A1*X1 != B1" << endl << endl << endl;
    } else
        cout << "Check ok" << endl << endl << endl;


    cout << "A COMPLEX<DOUBLE>, B COMPLEX<DOUBLE>" << endl << endl;
    Matrix_CD A2 = {
        {cplxd(1, 1), cplxd(1, 0)},
        {cplxd(0, 1), cplxd(4, 0)}
    };
    Matrix_CD B2 = {
        {cplxd(-1, 1)},
        {cplxd( 2, 4)}
    };
    MATRIX_PRINT(A2);
    cout << endl;
    MATRIX_PRINT(B2);
    cout << endl;
    MATRIX_PRINT(mldivide(A2,B2));
    cout << endl;

    auto X2 = mldivide(A2,B2);
    if (!almost_equal(A2*X2, B2)) {
        cout << "WARNING: A2*X2 != B2" << endl << endl << endl;
    } else
        cout << "Check ok" << endl << endl << endl;


    cout << "A DOUBLE, B COMPLEX<DOUBLE>" << endl << endl;
    Matrix_D A3 = Matrix_D::rand(5);
    Matrix_CD B3 = {
        {cplxd(-1, 1), cplxd(-1, 1)},
        {cplxd( 2, 4), cplxd( 2, 4)},
        {cplxd(-1, 1), cplxd(-1, 1)},
        {cplxd( 2, 4), cplxd( 2, 4)},
        {cplxd(-1, 1), cplxd(-7,-2)}
    };
    MATRIX_PRINT(A3);
    cout << endl;
    MATRIX_PRINT(B3);
    cout << endl;
    MATRIX_PRINT(mldivide(A3,B3));
    cout << endl;

    auto X3 = mldivide(A3,B3);
    if (!almost_equal(A3*X3, B3)) {
        cout << "WARNING: A3*X3 != B3" << endl << endl << endl;
    } else
        cout << "Check ok" << endl << endl << endl;


    cout << "A COMPLEX<DOUBLE>, B DOUBLE" << endl << endl;
    Matrix_CD A4 = A2;
    Matrix_D B4 = {
        {1,  3},
        {5,  2},
    };
    MATRIX_PRINT(A4);
    cout << endl;
    MATRIX_PRINT(B4);
    cout << endl;
    MATRIX_PRINT(mldivide(A4,B4));
    cout << endl;

    auto X4 = mldivide(A4,B4);
    if (!almost_equal(A4*X4, static_cast<Matrix_CD>(B4))) {
        cout << "WARNING: A4*X4 != B4" << endl << endl << endl;
    } else
        cout << "Check ok" << endl << endl << endl;


    cout << "A LONG DOUBLE, B LONG DOUBLE" << endl << endl;
    Matrix_LD A5 = {
        {-7,  7, 10, 8},
        { 8,  2,  3, 3},
        { 3, -1,  6, 7},
        { 7,  5, -2, 5},
    };
    Matrix_LD B5 = {
        {1, 0},
        {0, 2},
        {1, 1},
        {2, 0}
    };
    MATRIX_PRINT(A5);
    cout << endl;
    MATRIX_PRINT(B5);
    cout << endl;
    MATRIX_PRINT(mldivide(A5,B5));
    cout << endl;

    auto X5 = mldivide(A5,B5);
    if (!almost_equal(A5*X5, B5)) {
        cout << "WARNING: A5*X5 != B5" << endl << endl << endl;
    } else
        cout << "Check ok" << endl << endl << endl;
}


void test_right_division()
{
    cout << endl << endl << endl;
    cout << "TEST MATRIX RIGHT DIVISION (mrdivide, /)" << endl << endl;

    cout << "A DOUBLE, B DOUBLE" << endl << endl;
    Matrix_D A1 = {
        {1,  0,  0,  2},
        {1,  1,  2,  0},
        {0,  1,  0, -1}
    };
    Matrix_D B1 = {
        {-7,  7, 10, 8},
        { 8,  2,  3, 3},
        { 3, -1,  6, 7},
        { 7,  5, -2, 5},
    };
    MATRIX_PRINT(A1);
    cout << endl;
    MATRIX_PRINT(B1);
    cout << endl;
    MATRIX_PRINT(A1/B1);
    cout << endl;

    auto X1 = A1 / B1;
    if (!almost_equal(X1*B1, A1)) {
        cout << "WARNING: X1*B1 != A1" << endl << endl << endl;
    } else
        cout << "Check ok" << endl << endl << endl;


    cout << "A COMPLEX<DOUBLE>, B COMPLEX<DOUBLE>" << endl << endl;
    Matrix_CD A2 = {
        {cplxd(-1, 1), cplxd( 2, 4)},
        {cplxd(-1, 0), cplxd( 0, 2)}
    };
    Matrix_CD B2 = {
        {cplxd(1, 1), cplxd(1, 0)},
        {cplxd(0, 1), cplxd(4, 0)}
    };
    MATRIX_PRINT(A2);
    cout << endl;
    MATRIX_PRINT(B2);
    cout << endl;
    MATRIX_PRINT(A2/B2);
    cout << endl;

    auto X2 = A2 / B2;
    if (!almost_equal(X2*B2, A2)) {
        cout << "WARNING: X2*B2 != A2" << endl << endl << endl;
    } else
        cout << "Check ok" << endl << endl << endl;


    cout << "A DOUBLE, B COMPLEX<DOUBLE>" << endl << endl;
    Matrix_D A3 = {
        {1,  3},
        {9, -1},
        {0,  4},
    };
    Matrix_CD B3 = B2;
    MATRIX_PRINT(A3);
    cout << endl;
    MATRIX_PRINT(B3);
    cout << endl;
    MATRIX_PRINT(A3/B3);
    cout << endl;

    auto X3 = A3 / B3;
    if (!almost_equal(X3*B3, static_cast<Matrix_CD>(A3))) {
        cout << "WARNING: X3*B3 != A3" << endl << endl << endl;
    } else
        cout << "Check ok" << endl << endl << endl;


    cout << "A COMPLEX<DOUBLE>, B DOUBLE" << endl << endl;
    Matrix_CD A4 = {
        {cplxd(-1, 1), cplxd(-1, 1), cplxd( 2, 4), cplxd( 2, 4), cplxd(-1, 1)},
        {cplxd(-1, 0), cplxd( 2, 4), cplxd( 2, 4), cplxd(-1, 1), cplxd(-7,-2)},
        {cplxd(-1, 0), cplxd( 0, 4), cplxd( 2, 0), cplxd( 0,-1), cplxd( 2,-2)}
    };
    Matrix_D B4 = Matrix_D::rand(5);
    MATRIX_PRINT(A4);
    cout << endl;
    MATRIX_PRINT(B4);
    cout << endl;
    MATRIX_PRINT(A4/B4);
    cout << endl;

    auto X4 = A4 / B4;
    if (!almost_equal(X4*B4, A4)) {
        cout << "WARNING: X4*B4 != A4" << endl << endl << endl;
    } else
        cout << "Check ok" << endl << endl << endl;


    cout << "A LONG DOUBLE, B LONG DOUBLE" << endl << endl;
    Matrix_LD A5 = {
        {1, 0, 0, 2},
        {1, 1, 2, 0}
    };
    Matrix_LD B5 = {
        {-7,  7, 10, 8},
        { 8,  2,  3, 3},
        { 3, -1,  6, 7},
        { 7,  5, -2, 5},
    };
    MATRIX_PRINT(A5);
    cout << endl;
    MATRIX_PRINT(B5);
    cout << endl;
    MATRIX_PRINT(A5/B5);
    cout << endl;

    auto X5 = A5 / B5;
    if (!almost_equal(X5*B5, A5)) {
        cout << "WARNING: X5*B5 != A5" << endl << endl << endl;
    } else
        cout << "Check ok" << endl << endl << endl;
}


void test_matrix_inversion()
{
    bool error;
    cout << endl << endl << endl;
    cout << "TEST MATRIX INVERSE" << endl << endl;

    error = false;
    Matrix_D A = {
        {10.5}
    };
    MATRIX_PRINT(A);
    cout << endl;
    MATRIX_PRINT(inv(A));
    cout << endl;

    if (!almost_equal(A*inv(A), Matrix_D::eye(A.nrows()))) {
        cout << "WARNING: A*inv(A) != I" << endl << endl << endl;
        error = true;
    }
    if (!almost_equal(inv(A)*A, Matrix_D::eye(A.nrows()))) {
        cout << "WARNING: inv(A)*A != I" << endl << endl << endl;
        error = true;
    }
    if (!error)
        cout << "Check ok" << endl << endl << endl;


    error = false;
    Matrix_D B = {
        {1,  3},
        {5,  2},
    };
    MATRIX_PRINT(B);
    cout << endl;
    MATRIX_PRINT(inv(B));
    cout << endl;

    if (!almost_equal(B*inv(B), Matrix_D::eye(B.nrows()))) {
        cout << "WARNING: B*inv(B) != I" << endl << endl << endl;
        error = true;
    }
    if (!almost_equal(inv(B)*B, Matrix_D::eye(B.nrows()))) {
        cout << "WARNING: inv(B)*B != I" << endl << endl << endl;
        error = true;
    }
    if (!error)
        cout << "Check ok" << endl << endl << endl;


    error = false;
    Matrix_D C = {
        {2, 1, 3},
        {1, 0, 2},
        {2, 0,-2},
    };
    MATRIX_PRINT(C);
    cout << endl;
    MATRIX_PRINT(inv(C));
    cout << endl;

    if (!almost_equal(C*inv(C), Matrix_D::eye(C.nrows()))) {
        cout << "WARNING: C*inv(C) != I" << endl << endl << endl;
        error = true;
    }
    if (!almost_equal(inv(C)*C, Matrix_D::eye(C.nrows()))) {
        cout << "WARNING: inv(C)*C != I" << endl << endl << endl;
        error = true;
    }
    if (!error)
        cout << "Check ok" << endl << endl << endl;


    error = false;
    Matrix_D D = {
        {-7,  7, 10, 8},
        { 8,  2,  3, 3},
        { 3, -1,  6, 7},
        { 7,  5, -2, 5},
    };
    MATRIX_PRINT(D);
    cout << endl;
    MATRIX_PRINT(inv(D));
    cout << endl;

    if (!almost_equal(D*inv(D), Matrix_D::eye(D.nrows()))) {
        cout << "WARNING: D*inv(D) != I" << endl << endl << endl;
        error = true;
    }
    if (!almost_equal(inv(D)*D, Matrix_D::eye(D.nrows()))) {
        cout << "WARNING: inv(D)*D != I" << endl << endl << endl;
        error = true;
    }
    if (!error)
        cout << "Check ok" << endl << endl << endl;


    error = false;
    Matrix_CD E = {
        {cplxd(1, 1), cplxd(1,0), cplxd( 0,-1)},
        {cplxd(0,-1), cplxd(4,0), cplxd(-2, 0)},
        {cplxd(7, 0), cplxd(1,1), cplxd(-1,-1)}
    };
    MATRIX_PRINT(E);
    cout << endl;
    MATRIX_PRINT(inv(E));
    cout << endl;

    if (!almost_equal(E*inv(E), Matrix_CD::eye(E.nrows()))) {
        cout << "WARNING: E*inv(E) != I" << endl << endl << endl;
        error = true;
    }
    if (!almost_equal(inv(E)*E, Matrix_CD::eye(E.nrows()))) {
        cout << "WARNING: inv(E)*E != I" << endl << endl << endl;
        error = true;
    }
    if (!error)
        cout << "Check ok" << endl << endl << endl;


    error = false;
    MATRIX_PRINT(Matrix_D::eye(4));
    cout << endl;
    MATRIX_PRINT(inv(Matrix_D::eye(4)));
    cout << endl;

    if (!almost_equal(Matrix_D::eye(4)*inv(Matrix_D::eye(4)), Matrix_D::eye(4))) {
        cout << "WARNING: I*inv(I) != I" << endl << endl << endl;
        error = true;
    }
    if (!almost_equal(inv(Matrix_D::eye(4))*Matrix_D::eye(4), Matrix_D::eye(4))) {
        cout << "WARNING: inv(I)*I != I" << endl << endl << endl;
        error = true;
    }
    if (!error)
        cout << "Check ok" << endl << endl << endl;


    error = false;
    MATRIX_PRINT(Matrix_D::hilb(4));
    cout << endl;
    MATRIX_PRINT(inv(Matrix_D::hilb(4)));
    cout << endl;

    // In compare we must increase the epsilon magnification factor, because
    // Hilbert matrices are close to being singular which make them difficult
    // to invert with numerical routines (a particular formula to calculate the
    // Hilbert inverse exists, see 'invhilb' in Octave). So the product of hilb()
    // whith (inv(hilb()) produces bigger approssimations than non singular matrices.
    if (!almost_equal(Matrix_D::hilb(4)*inv(Matrix_D::hilb(4)), Matrix_D::eye(4), 10000)) {
        cout << "WARNING: hilb(4)*inv(hilb(4)) != I" << endl << endl << endl;
        error = true;
    }
    if (!almost_equal(inv(Matrix_D::hilb(4))*Matrix_D::hilb(4), Matrix_D::eye(4), 10000)) {
        cout << "WARNING: inv(hilb(4))*hilb(4) != I" << endl << endl << endl;
        error = true;
    }
    if (!error)
        cout << "Check ok" << endl << endl << endl;


    error = false;
    MATRIX_PRINT(Matrix_D::zeros(4));
    cout << endl;
    MATRIX_PRINT(inv(Matrix_D::zeros(4)));      // Here an exception is generated
    cout << endl << endl;
}


void test_determinant()
{
    cout << endl << endl << endl;
    cout << "TEST MATRIX DETERMINANT" << endl << endl;

    Matrix_D A = {
        {10.5}
    };
    MATRIX_PRINT(A);
    cout << endl << "det(A) = " << det(A) << endl << endl << endl;

    Matrix_D B = {
        {1,  3},
        {5,  2},
    };
    MATRIX_PRINT(B);
    cout << endl << "det(B) = " << det(B) << endl << endl << endl;

    Matrix_D C = {
        {2, 1, 3},
        {1, 0, 2},
        {2, 0,-2},
    };
    MATRIX_PRINT(C);
    cout << endl << "det(C) = " << det(C) << endl << endl << endl;

    Matrix_D D = {
        {-7,  7, 10, 8},
        { 8,  2,  3, 3},
        { 3, -1,  6, 7},
        { 7,  5, -2, 5},
    };
    MATRIX_PRINT(D);
    cout << endl << "det(D) = " << det(D) << endl << endl << endl;

    Matrix_CD E = {
        {cplxd(1, 1), cplxd(1,0), cplxd( 0,-1)},
        {cplxd(0,-1), cplxd(4,0), cplxd(-2, 0)},
        {cplxd(7, 0), cplxd(1,1), cplxd(-1,-1)}
    };
    MATRIX_PRINT(E);
    cout << endl << "det(E) = " << det(E) << endl << endl << endl;

    MATRIX_PRINT(Matrix_D::eye(4));
    cout << endl << "det(Matrix_D::eye(4)) = " << det(Matrix_D::eye(4)) << endl << endl;

    MATRIX_PRINT(Matrix_D::hilb(4));
    cout << endl << "det(Matrix_D::hilb(4)) = " << det(Matrix_D::hilb(4)) << endl << endl;
 }

