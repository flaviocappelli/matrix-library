﻿/*
 * Simple matrix template class in C++14
 * (C) Flavio Cappelli 2018 - ROME (IT)
 * All rights reserved
 *
 * Published under the Modified BSD License.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer listed
 *   in this license in the documentation and/or other materials
 *   provided with the distribution.
 *
 * - Neither the name of the copyright holders nor the names of its
 *   contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * The copyright holders provide no reassurances that the source code
 * provided does not infringe any patent, copyright, or any other
 * intellectual property rights of third parties. The copyright holders
 * disclaim any liability to any recipient for claims brought against
 * recipient by any third party for infringement of that parties
 * intellectual property rights.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
 * BRIEF DESCRIPTION OF THE MATRIX CLASS
 * =====================================
 *
 * This class uses the "reference counting" and the "copy on write"
 * techniques to minimize copy operations. The data is stored inside
 * a container together with the number of allocated rows and columns,
 * plus a reference counter. When we make a copy of the matrix (direct
 * or through a copy constructor) only the pointer to the container is
 * copied and the reference counter is incremented. When the matrix is
 * destroyed, the reference counter is decremented and if it reaches 0
 * then the container is also destroyed and the memory released (because
 * there are not other matrices that point to such container). When we
 * modify one or more elements of the matrix, that point to a container
 * whose reference counter is greater than 1, the container is copied,
 * the reference counter of the copy is set to 1, and such copy assigned
 * to the matrix on which we are writing, while the reference counter of
 * the original container is decremented. Thanks to these techniques, we
 * can copy or pass a Matrix object by value with minimal overhead.
 *
 * This class has essentially educational purposes. On the net there are
 * other libraries that are better optimized for the modern architectures.
 * Anyway some operations of this class use BLAS / LAPACK whenever possible
 * (matrix product, LU decomposition, solve of AX=B system, matrix inverse).
 * An interesting thing about this implementation is that it is possible to
 * perform some operations on matrices of different types, for example add
 * a matrix with double values to a matrix with complex long double values.
 * The result type is automatically determined by the rules for promoting
 * types in expressions (using the keyword decltype of the C++11). However,
 * when matrices have different types some BLAS optimizations cannot be used.
 * The matrix class uses templates and can be instantiated on any numerical
 * type, but the BLAS routines only support float, double, complex<float>,
 * complex<double> data types, so they cannot be used with long doubles or
 * integers. Also, some member function are not avaiable for integer types.
 * The implementation partially resemble the style of Matlab / Octave.
 *
 * C++ features and techniques used in the development of this class
 * -----------------------------------------------------------------
 *  - rvalue references, move semantics, reference qualifiers
 *  - specialization of template members and functions
 *  - implicit sharing and copy-on-write
 *  - initialization lists, range for
 *  - auto and decltype keywords
 *  - SFINAE ("Substitution Failure Is Not An Error")
 *  - proxy design pattern
 *
 * Used libraries
 * --------------
 *  - BLAS/LAPACK (optional, OpenBLAS + Reference LAPACK suggested)
 *  - BOOST (required, headers only, no link to boost libraries needed)
 *
 * Limitations
 * -----------
 *  - only 2-dimensional matrices (or simple vectors) can be created;
 *  - on 64bit environments, rows and columns are both limited to a maximum of 46340
 *    because their product must not exceed INT_MAX; instead, on a 32bit environment
 *    they are limited to 11585, because max_nrows * max_ncols * 32 must not exceed
 *    SIZE_MAX (32 is the size of the biggest data type, i.e. complex<long double>);
 *  - few math functions are implemented (but the class can be easily extended).
 *
 * MATRIX/VECTOR TYPES
 * -------------------
 * Matrix_I      RowVec_I      ColVec_I        int
 * Matrix_F      RowVec_F      ColVec_F        float
 * Matrix_D      RowVec_D      ColVec_D        double
 * Matrix_LD     RowVec_LD     ColVec_LD       lonf double
 * Matrix_CI     RowVec_CI     ColVec_CI       complex<int>
 * Matrix_CF     RowVec_CF     ColVec_CF       complex<float>
 * Matrix_CD     RowVec_CD     ColVec_CD       complex<double>
 * Matrix_CLD    RowVec_CLD    ColVec_CLD      complex<lonf double>
 *
 * FUNCTIONS AND OPERATORS IMPLEMENTED - SEE ALSO "test_matrix.cxx"
 * ----------------------------------------------------------------
 * Constructors:
 *
 *     Matrix_*()
 *     Matrix_*(nrows, ncols)
 *     Matrix_*(nrows, const, *array)
 *     Matrix_*(nrows, ncols, {...})
 *     Matrix_*({{...},{...},...})
 *     Matrix_*(v)
 *
 * Non static member functions and operators (M is the matrix object):
 *
 *     =                       [copy/move assignment]
 *     M(i,j)                  [operator()(i,j) to read/write matrix elements]
 *     +, -                    [unary +() e -() operators]
 *     static_cast             [for a 1x1 matrix, toward the stored T type]
 *     M.isnull()              [return true if null, i.e. no data allocated]
 *     M.nrows()               [return the n.of.rows of the matrix, or 0 if null]
 *     M.ncols()               [return the n.of.columns of the matrix, or 0 if null]
 *     M.tran()                [return the simple transpose (i.e. not conjugate)]
 *     M.conj()                [return the complex conjugate of the matrix]
 *     M.ctran()               [return the complex conjugate transpose]
 *     M.real()                [return the real part of the complex matrix]
 *     M.imag()                [return the imaginary part of the complex matrix]
 *     M.submat(r1,c1,r2,c2)   [extract the submatrix of indexes r1,c1,r2,c2]
 *     M.resize(nrows,ncols)   [resize the matrix, become null with (0,0)]
 *
 * Static member functions:
 *
 *     eye(n)                  [return a square NxN identity matrix]
 *     ones(n)                 [return a matrix NxN or MxN whose elements are all 1]
 *     ones(m,n)
 *     zeros(n)                [return a matrix NxN or MxN whose elements are all 0]
 *     zeros(m,n)
 *     rand(n)                 [ONLY REAL MATRICES: return a matrix NxN or MxN with
 *     rand(m,n)                uniformly distributed elements in range 0.0 - 1.0]
 *     rand(n,l,u)             [as above but specify lower and upper range values]
 *     rand(m,n,l,u)
 *     randn(n)                [ONLY REAL MATRICES: return a matrix NxN or MxN with
 *     randn(m,n)               normally distributed elements (mean 0.0, stddev 1.0)]
 *     randn(n,me,sd)          [as above but specify mean and standard deviation]
 *     randn(m,n,me,sd)
 *     hilb(n)                 [return the Hilbert matrix of order N (see the function)]
 *
 * Other operators and functions (A,B matrices, v scalar (integer, real or complex)):
 *
 *     A+v,  v+A               [sum a scalar value to all elements of the matrix]
 *     A+B                     [sum two matrices (of same size) element by element]
 *     A-v                     [subtract a scalar value from all elements of the matrix]
 *     v-A                     [invert the sign of the matrix and add a value to all elements]
 *     A-B                     [subtract two matrices (of same size) element by element]
 *     array_mul(A,v),  A*v    [multipy a scalar value to all elements of the matrix]
 *     array_mul(v,A),  v*A    [as above]
 *     array_mul(A,B)          [multiply two matrices (of same size) element by element]
 *     array_div(A,v),  A/v    [divide all elements of the the matrix by a scalar value]
 *     array_div(v,A)          [for all elements of the matrix: A(i,j) = v/A(i,j)]
 *     array_div(A,B)          [divide two matrices (of same size) element by element]
 *     A*B                     [matrix product rows by columns (BLAS optimized if possible)]
 *     LU_dec(A,Y)             [LU decomposition (LAPACK optimized), return Y as resulf of LAPACK GETRF]
 *     LU_dec(A,L,U)           [LU decomposition (LAPACK optimized), return L and U so that A = LU]
 *     LU_dec(A,L,U,P)         [LU decomposition (LAPACK optimized), return L,U,P so that P*A = L*U]
 *     inv(M)                  [inverse, matrix must be square not singular]
 *     mldivide(A,B)           [left division (A square), conceptually equivalent to inv(A)*B]
 *     linsolve(A,B)           [solve the linear system AX = B, it's the same of mldivide(A,B)]
 *     mrdivide(A,B),  A/B     [right division (B square), conceptually equivalent to A*inv(B)]
 *     v/A                     [equivalent to (v*I)/A, v scalar, I identity matrix, A square]
 *     det(M)                  [determinant]
 *
 *
 * THIS LIBRARY HAS BEEN TESTED ONLY ON LINUX, BUT IT SHOULD WORK IN ANY ENVIRONMENT.
 * TO DROP ANY DEPENDENCY FROM BLAS/LAPACK, DEFINE THE MACRO "NOBLASLAPACK" BEFORE
 * INCLUDING THIS FILE (OF COURSE THE BLAS/LAPACK OPTIMIZATIONS WILL BE LOST).
 */

#ifndef MATRIX_HXX
#define MATRIX_HXX

#if __cplusplus < 201402L
#  error "Sorry, this class needs some C++11 and C++14 features"
#endif

#define MATRIX_STRINGIFY2(x)    #x
#define MATRIX_STRINGIFY(x)     MATRIX_STRINGIFY2(x)
#define MATRIX_VOID_CAST        static_cast<void>

#include <climits>
#include <cstdint>

// Determine the Arch and set the max allowed number of rows and columns.
#if defined(__LP64__) || defined(_WIN64) \
 || (defined(__x86_64__) && !defined(__ILP32__)) \
 || defined(_M_X64) || defined(__ia64) || defined (_M_IA64) \
 || defined(__aarch64__) || defined(__powerpc64__)
// 64-bit Arch
static constexpr int max_nrows = 46340;
static constexpr int max_ncols = 46340;
static_assert(sizeof(void *) == 8, "error: the Arch is not what I think it is");
#else
// 32-bit Arch.
static constexpr int max_nrows = 11585;
static constexpr int max_ncols = 11585;
static_assert(sizeof(void *) == 4, "error: the Arch is not what I think it is");
#endif

// Check allocation limits: 32 is the size of the biggest type (complex<long double>).
static_assert(static_cast<unsigned long long>(max_nrows)*max_ncols <= static_cast<unsigned long long>(INT_MAX), "error: invalid max_nrows or max_ncols");
static_assert(static_cast<unsigned long long>(max_nrows)*max_ncols*32 <= static_cast<unsigned long long>(SIZE_MAX), "error: invalid max_nrows or max_ncols");



// //////////////////////////////////////////////////////////////////////////////// //
//                                Matrix Exceptions                                 //
// //////////////////////////////////////////////////////////////////////////////// //

#include <string>
#include <exception>
#include <boost/type_index.hpp>     // See macros below.


class matrixException : public std::exception
{
public:
    matrixException(const std::string & func, const char *file, const int line, const char *msg)
    {
        const int n_spc = 11;
        message.append("matrixException\n");
        message.append(n_spc, ' ').append("ASSERT FAILED : ").append(msg).append("\n");
        message.append(n_spc, ' ').append("Function      : ").append(func).append("\n");
        message.append(n_spc, ' ').append("File          : ").append(file).append("\n");
        message.append(n_spc, ' ').append("Line          : ").append(std::to_string(line)).append("\n");   // C++11
    }

    virtual const char* what() const noexcept
    {
        return message.c_str();
    }

private:
    std::string message;
};

#define MATRIXBASE_ASSERT(expr,args,msg)                                        \
    if(!(expr)) {                                                               \
        throw matrixException(                                                  \
            boost::typeindex::type_id<MatrixBase<T>>().pretty_name()            \
            .append("::")                                                       \
            .append(__FUNCTION__)                                               \
            .append("(")                                                        \
            .append(args)                                                       \
            .append(")")                                                        \
            , __FILE__, __LINE__, msg);                                         \
    }

#define MATRIX_BINARY_OPMM_ASSERT(expr,msg)                                     \
    if(!(expr)) {                                                               \
        throw matrixException(                                                  \
            std::string(__FUNCTION__)                                           \
            .append("(")                                                        \
            .append(boost::typeindex::type_id<decltype(A)>().pretty_name())     \
            .append(", ")                                                       \
            .append(boost::typeindex::type_id<decltype(B)>().pretty_name())     \
            .append(")")                                                        \
            , __FILE__, __LINE__, msg);                                         \
    }

#define MATRIX_BINARY_OPMV_ASSERT(expr,msg)                                     \
    if(!(expr)) {                                                               \
        throw matrixException(                                                  \
            std::string(__FUNCTION__)                                           \
            .append("(")                                                        \
            .append(boost::typeindex::type_id<decltype(M)>().pretty_name())     \
            .append(", ")                                                       \
            .append(boost::typeindex::type_id<decltype(v)>().pretty_name())     \
            .append(")")                                                        \
            , __FILE__, __LINE__, msg);                                         \
    }

#define MATRIX_BINARY_OPVM_ASSERT(expr,msg)                                     \
    if(!(expr)) {                                                               \
        throw matrixException(                                                  \
            std::string(__FUNCTION__)                                           \
            .append("(")                                                        \
            .append(boost::typeindex::type_id<decltype(v)>().pretty_name())     \
            .append(", ")                                                       \
            .append(boost::typeindex::type_id<decltype(M)>().pretty_name())     \
            .append(")")                                                        \
            , __FILE__, __LINE__, msg);                                         \
    }



// //////////////////////////////////////////////////////////////////////////////// //
//                                 Debugging macros                                 //
// //////////////////////////////////////////////////////////////////////////////// //

#ifdef  NDEBUG
#  define MATRIX_DUMP_INTERNAL(matrix,dump)                 (MATRIX_VOID_CAST(0))
#else
#  define MATRIX_DUMP_INTERNAL(matrix,dump_data)                                \
    matrix.dump_internal(MATRIX_STRINGIFY(matrix),dump_data)
#endif



// //////////////////////////////////////////////////////////////////////////////// //
//                                   Print macros                                   //
// MATRIX_PRINT(A)                                                                  //
//   display the string "A = " followed by the formatted matrix elements            //
//                                                                                  //
// MATRIX_PRINT(A, std::scientific, 6)                                              //
//   as above, but use the scientific notation (default fixed)                      //
//   and set the floating point precision to 6 decimal digit (default 4)            //
// //////////////////////////////////////////////////////////////////////////////// //

// See https://stackoverflow.com/questions/11761703

#define MATRIX_PRINT_HLP1(m)        (m).print(MATRIX_STRINGIFY(m)" = ")
#define MATRIX_PRINT_HLP2(m,f)      (m).print(MATRIX_STRINGIFY(m)" = ",f)
#define MATRIX_PRINT_HLP3(m,f,p)    (m).print(MATRIX_STRINGIFY(m)" = ",f,p)

#define MATRIX_EXPAND(x)            x
#define MATRIX_GET_MACRO(_1,_2,_3,NAME,...) NAME
#define MATRIX_PRINT(...)           MATRIX_EXPAND(MATRIX_GET_MACRO(__VA_ARGS__, \
                                    MATRIX_PRINT_HLP3, MATRIX_PRINT_HLP2,       \
                                    MATRIX_PRINT_HLP1)(__VA_ARGS__))



// //////////////////////////////////////////////////////////////////////////////// //
//                                 MatrixBase class                                 //
// //////////////////////////////////////////////////////////////////////////////// //

#include <initializer_list>         // Use std::initializer_list
#include <type_traits>              // Use std::is_floating_point<T>
#include <iostream>                 // Use std::ostream, std::cerr
#include <iomanip>                  // Use std::ios_base, std::fixed, std:scientific,...

#include "matrix_cplx.hxx"          // Use my type traits and complex types.


template <typename T>               // Derived class for row vectors.
class RowVecBase;


template <typename T>               // Derived class for column vectors.
class ColVecBase;


template <typename T>               // Proxy class: must be declared in advance.
class MatrixBaseRef;                // Return type in MatrixBase::operator(i,j), see below.


template <typename T>
class MatrixBase
{
public:
    // **************************** CTORS AND DTOR ******************************

    // Create a null matrix.
    inline MatrixBase() noexcept;

    // Destroy the matrix (note: the container is
    // destroyed only if 'reference counter' == 1).
    inline ~MatrixBase() noexcept;

    // Create a matrice  nrows x ncols (initialized with all 0 if 'init'is true).
    inline MatrixBase(const int nrows, const int ncols, const bool init = true);

    // Initialize the matrix using a unique initializer list. The sequence
    // of values is expected organized by rows (but internally it's stored by
    // columns for compatibility with BLAS/LAPACK, see the 'container' class).
    // The list must have nrows * ncols values, otherwise an exception is raised.
    inline MatrixBase(const int nrows, const int ncols, const std::initializer_list<T> & list);

    // Initialize the matrix using an initializer list whose elements
    // are themselves initializer lists (rows of the matrix). Raise an
    // exception if the rows (lists) have a different number of elements.
    inline MatrixBase(const std::initializer_list< std::initializer_list<T>> & list);

    // Initialize the matrix with an array of matching types. The sequence
    // of values is expected organized by rows (but internally it's stored by
    // columns for compatibility with BLAS/LAPACK, see the 'container' class).
    // The array must have enough data. Raises an exception if 'array' is null.
    inline MatrixBase(const int nrows, const int ncols, T *array);

    // Create explicitly a matrix 1x1.
    inline explicit MatrixBase(const T v);

    // Copy and move constructors.
    inline MatrixBase(const MatrixBase<T> & M);
    inline MatrixBase(MatrixBase<T> && M) noexcept;

    // **************************** CLASS OPERATORS *****************************

    // Copy and move assignment.
    inline MatrixBase<T> & operator=(const MatrixBase<T> & M);
    inline MatrixBase<T> & operator=(MatrixBase<T> && M) noexcept;

    // Access the matrix elements using the (i,j) position. The
    // MatrixBaseRef (proxy) class is used to distinguish read from
    // write accesses. In write accesses the class perform a detach
    // of the container when required, see the MatrixBaseRef class.
    inline MatrixBaseRef<T> operator()(const int i, const int j);

    // If the matrix is const, only read is possible (no detach).
    inline T operator()(const int i, const int j) const;

    // Unary operators + and -.
    inline MatrixBase<T> operator+() const;
    inline MatrixBase<T> operator-() const;

    // Explicit casting, with static_cast<T>(), towards the T type. The
    // matrix must have 1x1 dimension, otherwise an exception is raised.
    inline explicit operator T() const;

    // Explicit casting, with static_cast<MatrixBase<T>>(), toward the
    // MatrixBase<T> type. The casting must be allowed from type T to S.
    // This member perform a complete copy of the matrix, so it must be
    // enabled only when the type S and T are different (when they are the
    // same, the normal copy constructor or move constructor is istantiated).
    template<typename S, typename = std::enable_if<!std::is_same<T,S>::value>>
    inline explicit MatrixBase(const MatrixBase<S> & M);

    // ************************** OTHER PUBLIC MEMBERS **************************

    // TODO
    // - A().norm(p)
    // - A().cond(p = 2) by definition is  norm(A, P) * norm (inv(A), P)
    //   but must be calculated using Single Value Decomposition, see PDF

    // Return true if the matrix is null (i.e. no container allocated).
    inline bool isnull() const noexcept;

    // Return the number of rows of the matrix (0 if the matrix is null).
    inline int nrows() const noexcept;

    // Return the number of columns of the matrix (0 if the matrix is null).
    inline int ncols() const noexcept;

    // Return the transpose of the matrix
    // (not the complex conjugate transpose).
    inline MatrixBase<T> tran() const;

    // Return the complex conjugate of the matrix (if
    // the matrix is real, the same object is returned).
    inline MatrixBase<T> conj() const;

    // Return the complex conjugate transpose of the matrix (if
    // the matrix is real, the same result of tran() is returned).
    inline MatrixBase<T> ctran() const;

    // ONLY COMPLEX MATRICES (complex<float>, complex<double>,
    // complex<long double>). Return the real part of the matrix.
    template <typename S = T, typename = std::enable_if<std::is_complex<T>::value>>
    inline auto real() const -> MatrixBase<typename get_complex_subtype<S>::type>;

    // ONLY COMPLEX MATRICES (complex<float>, complex<double>,
    // complex<long double>). Return the imaginary part of the matrix.
    template <typename S = T, typename = std::enable_if<std::is_complex<T>::value>>
    inline auto imag() const -> MatrixBase<typename get_complex_subtype<S>::type>;

    // Extract the submatrix (r1, c1, r2, c2). The indexes are sorted, so
    // that r1 <= r2 and c1 <= c2 but they must be valid (i.e. in the range
    // of allocated rows and columns) otherwise an exception is generated.
    inline MatrixBase<T> submat(int r1, int c1, int r2, int c2) const;

    // Resize the matrix: if BOTH 'nrows' AND 'ncols' parameters are 0,
    // the matrix become null; if the matrix is enlarged, the additional
    // elements are initialized to T(0) if the "init" parameter is true.
    inline void resize(const int nrows, const int ncols, const bool init = true);

    // Formatted output of the matrix data. See also the 'MATRIX_PRINT()' macro.
    inline void print(const char *preamble, std::ios_base &(*iomanip)(std::ios_base &) = std::fixed, const int fp_prec=4) const;
    void print(std::ostream & out, const char *preamble, std::ios_base &(*iomanip)(std::ios_base &) = std::fixed, const int fp_prec=4) const;

    // Helper functions used to make NON member functions
    // and operators more efficient. ASSUME MATRIX NOT NULL.
    inline int  nr_unsafe() const noexcept;
    inline int  nc_unsafe() const noexcept;
    inline int  cnt_unsafe() const noexcept;
    inline   T  get_unsafe(const int k) const noexcept;
    inline   T  get_unsafe(const int i, const int j) const noexcept;
    inline void set_unsafe(const int k, const T v) noexcept;
    inline void set_unsafe(const int i, const int j, const T v) noexcept;
    inline  T & ref_unsafe(const int i, const int j) noexcept;

    // Helper functions: constant references to the number of
    // rows, number of column and start of matrix data. Used by
    // BLAS/LAPACK Fortran interface. ASSUME MATRIX NOT NULL.
    inline const int *blas_ref_nr() const noexcept;
    inline const int *blas_ref_nc() const noexcept;
    inline const   T *blas_ref_ma() const noexcept;

    // Helper function: return a detached copy of the matrix (casted to
    // MatrixBase<S> if required). Casting must be allowed from T to S.
    // This member is used inside  other member functions and operators.
    template<typename S>
    inline MatrixBase<S> casted_copy() const;

    // Helper function called within the 'MATRIX_DUMP_INTERNAL()' macro, used
    // for debugging. DO NOT CALL THIS FUNCTION, BUT USE ALWAYS THE ABOVE MACRO.
    // NOTE: the matrix elements are shown as stored in memory (COLUMN MAJOR).
    inline void dump_internal(const char *name, bool dump_data) const;

    // **************************** STATIC MEMBERS ******************************

    // Return a square NxN identity matrix.
    static inline MatrixBase<T> eye(const int n);

    // Return a matrix NxN or NxN whose elements are all 1.
    static inline MatrixBase<T> ones(const int n);
    static inline MatrixBase<T> ones(const int m, const int n);

    // Return a matrix NxN or MxN whose elements are all 0.
    static inline MatrixBase<T> zeros(const int n);
    static inline MatrixBase<T> zeros(const int m, const int n);

    // ONLY REAL MATRICES (float, double, long double).
    // Return a matrix NxN or MxN with uniformly distributed
    // random elements (on the specified interval, default [0,1]).
    static inline MatrixBase<T> rand(const int n, const T lower = 0, const T upper = 1);
    static inline MatrixBase<T> rand(const int m, const int n, const T lower = 0, const T upper = 1);

    // ONLY REAL MATRICES (float, double, long double).
    // Return a matrix NXN or MxN with normally distributed random elements
    // (having specified mean and standard deviation, default 0.0 and 1.0).
    static inline MatrixBase<T> randn(const int n, const T mean = 0, const T devstd = 1);
    static inline MatrixBase<T> randn(const int m, const int n, const T mean = 0, const T devstd = 1);

    // Return the Hilbert matrix of order N. The i,j element of a Hilbert matrix
    // is defined as H(i,j) = 1 / ((i+1) + (j+1) - 1) (with i, j indexes starting
    // from zero). Hilbert matrices are close to being singular which make them
    // difficult to invert with numerical routines (can be used for testing).
    static inline MatrixBase<T> hilb(const int n);

protected:
    // Helper function: generate random matrix with the selected distribution.
    template <typename S>
    static inline MatrixBase<T> create_random_matrix(S & dist, const int n, const int m);

    // Class container, implicitly shared using the 'reference counting' and the
    // 'copy-on-write'. Data is stored as COLUMN MAJOR for BLAS/LAPACK compatibility.
    //
    // NOTE: for maximum efficiency I used a variables size structure to avoid the run-time
    // execution cost of a double pointer dereference. Because the "flexible arrays members"
    // (standardized in C99) are not part of the C++ (even in C++17), I used the classic hack
    // to have a structure with an array of just one element as the last member, and performed
    // alloc/destroy using malloc and free. All memory management is embedded in the container
    // class, and the internal structure of the container is not exposed outside.
    class container
    {
    public:
        // Return the number of rows and columns.
        inline int nr() const noexcept;
        inline int nc() const noexcept;

        // Return the reference counter.
        inline int cnt() const noexcept;

        // Get a data element from the container, using the [k] and (i,j)
        // notations. Note that there is no verification of the indexes.
        inline T get(const int k) const noexcept;
        inline T get(const int i, const int j) const noexcept;

        // Set a data element into the container, using the [k] and (i,j) notations.
        // NOTE that there is no verification of the indexes; also, there is no detach
        // performed (the container must be explicitly detached in advance if required).
        inline void set(const int k, const T v) noexcept;
        inline void set(const int i, const int j, const T v) noexcept;

        // Return a reference to the element at (i,j) indexes.
        // NOTE that there is no verification of the indexes.
        inline T & ref(const int i, const int j) noexcept;

        // Return a const reference to the number of rows and columns.
        // These are required by the Fortran interface of BLAS/LAPACK.
        inline const int & ref_nr() const noexcept;
        inline const int & ref_nc() const noexcept;

        // STATIC MEMBER. Allocate the container (including
        // the required memory for the matrix elements) and:
        //  - initialize the 'reference counter' to 1
        //  - store the number of rows (nr) and columns (nc)
        //  - initialize the matrix elements to 0 if "init" is true
        //  - return the starting address of the container if all is ok.
        // Raise a std::bad_alloc exception if memory cannot be allocated.
        // NOTE: there is no verification about the validity of parameters
        // rows and columns (this must be performed by MatrixBase members).
        inline static container * alloc(const int nr, const int nc, const bool init);

        // STATIC MEMBER. Copy the container and return the address of the copy
        // (simply increment the reference counter and return the same pointer).
        inline static container * copy(container * const p) noexcept;

        // STATIC MEMBER. Move the container (simply return
        // the address and set the original pointer to NULLPTR).
        inline static container * move(container *&p) noexcept;

        // STATIC MEMBER. Detach a container if required,
        // i.e. if the 'reference counter' is > 1
        //  - decrement the 'reference counter'
        //  - perform a full copy of the container
        //  - update the pointer with the address of the new container
        // Raise a std::bad_alloc exception if memory cannot be allocated.
        // NOTE: if the 'reference counter' is NOT > 1 do nothing.
        inline static void detach(container * &p);

        // STATIC MEMBER. Destroy the container, i.e.:
        //  - decrement the 'reference counter'
        //  - remove the container if the 'reference counter' is 0
        //  - in any case set the provided pointer to NULLPTR.
        inline static void destroy(container *&p) noexcept;

        // USED FOR DEBUG. Remember that the elements are stored
        // as COLUMN MAJOR for compatibility with BLAS/LAPACK!
        inline void dump_internal(bool dump_data) const;

    private:
        int c_nr;           // Number of matrix rows.
        int c_nc;           // Number of matrix columns.
        int c_cnt;          // Reference counter of the container
        T   c_data[1];      // THIS MUST BE THE LAST MEMBER. DO NOT CHANGE.

        // Constructor and destructor are not allowed. The container
        // management must be performed only using the above members.
        inline container();
        inline ~container();

        // Copy/move constructors and assignments are not allowed.
        inline container(const container &);
        inline container(container &&) noexcept;
        inline container & operator=(const container &);
        inline container & operator=(container &&) noexcept;
    } * m_d;

    // Row and column vector classes.
    friend class RowVecBase<T>;
    friend class ColVecBase<T>;

    // The MatrixBaseRef class is used to distinguish the read
    // access to the elements of the matrix from the write access.
    friend class MatrixBaseRef<T>;

    // This is required to access Tmp.m_d variable in members real()
    // and imag(), because in such members the MatrixBase Tmp is not of
    // type T but of subtype S (T = complex<S>), so here we must declare
    // friendship for all MatrixBase objects, regardles of the type.
    template <typename U>
    friend class MatrixBase;
};



// This class is a mediator for MatrixBase class (proxy pattern).
// MatrixBaseRef class: allows to distinguish when the MatrixBase
// operator()(i,j), is used on the left of an assignment and when
// is used on the right. With this distinction it is possible to
// perform different actions in reading and writing elements.
template <typename T>
class MatrixBaseRef
{
public:
    // Constructor: invoked by the operator()(i, j) on the MatrixBase
    // objects. It stores the reference to the object and the indexes.
    inline MatrixBaseRef(MatrixBase<T> & M, const int i, const int j);

    // Member called when we write to an element (i, j) of the matrix:
    // the operator()(i, j) creates a 'MatrixBaseRef' object, and this is
    // the type the assignment acts on (because it is on the left of the
    // assignment). Inside we perform what is required to modify the matrix.
    inline MatrixBaseRef & operator=(T val);

    // Member called when we read from an element (i, j) of a matrix:
    // the operator()(i, j) creates a 'MatrixBaseRef' object, that inside
    // an expression it is automaticaly converted to <T> by the compiler, via
    // this member. Inside we perform what is required to read the matrix.
    inline operator T();

private:
    class MatrixBase<T> & m_ref;    // MatrixBase object on which we want to read/write.
    int m_i, m_j;                   // Position (i,j) of the element we want to read/write.
};



// ------------------------- Container implementation  ---------------------------- //

#include <cstdlib>                  // Use malloc(), free()
#include <cstring>                  // Use memset(), memcpy()
#include <new>                      // Use std::bad_alloc()


template <typename T>
inline int MatrixBase<T>::container::nr() const noexcept
{
    return c_nr;
}


template <typename T>
inline int MatrixBase<T>::container::nc() const noexcept
{
    return c_nc;
}


template <typename T>
inline int MatrixBase<T>::container::cnt() const noexcept
{
    return c_cnt;
}


template <typename T>
inline T MatrixBase<T>::container::get(const int k) const noexcept
{
    return c_data[k];
}


template <typename T>
inline void MatrixBase<T>::container::set(const int k, const T v) noexcept
{
    c_data[k] = v;
}


template <typename T>
inline T MatrixBase<T>::container::get(const int i, const int j) const noexcept
{
    return c_data[j * c_nr + i];            // COLUMN MAJOR.
}


template <typename T>
inline void MatrixBase<T>::container::set(const int i, const int j, const T v) noexcept
{
    c_data[j * c_nr + i] = v;               // COLUMN MAJOR.
}


template <typename T>
inline T & MatrixBase<T>::container::ref(const int i, const int j) noexcept
{
    return c_data[j * c_nr + i];            // COLUMN MAJOR.
}


template <typename T>
inline const int & MatrixBase<T>::container::ref_nr() const noexcept
{
    return c_nr;
}


template <typename T>
inline const int & MatrixBase<T>::container::ref_nc() const noexcept
{
    return c_nc;
}


template <typename T>
inline typename MatrixBase<T>::container * MatrixBase<T>::container::alloc(const int nr, const int nc, const bool init)
{
    container *p = (container *) std::malloc(sizeof(container) + sizeof(T) * (nr * nc - 1));
    if (!p) throw std::bad_alloc();

    p->c_nr = nr;
    p->c_nc = nc;
    p->c_cnt = 1;

    if (init) {
        #ifdef __STDC_IEC_559__
        // Safe to use memset() when IEEE754 math is used.
        // See https://stackoverflow.com/questions/4629853
        std::memset(p->c_data, 0, sizeof(T) * (nr * nc));
        #else
        for (int i = 0; i < (nr * nc); p->c_data[i++] = 0);
        #endif
    }

    return p;
}


template<typename T>
inline typename MatrixBase<T>::container * MatrixBase<T>::container::copy(container * const p) noexcept
{
    if (p)
        ++(p->c_cnt);

    return p;
}


template<typename T>
inline typename MatrixBase<T>::container * MatrixBase<T>::container::move(container *&p) noexcept
{
    container *q = p;
    p = nullptr;
    return q;
}


template<typename T>
inline void MatrixBase<T>::container::detach(container *&p)
{
    if (p && p->c_cnt > 1) {
        --(p->c_cnt);

        container *q = container::alloc(p->c_nr, p->c_nc, false);
        std::memcpy(q->c_data, p->c_data, sizeof(T) * (p->c_nr * p->c_nc));

        p = q;
    }
}


template <typename T>
inline void MatrixBase<T>::container::destroy(container *&p) noexcept
{
    if (p) {
        if (--(p->c_cnt) == 0)
            std::free(p);
        p = nullptr;
    }
}


template <typename T>
inline void MatrixBase<T>::container::dump_internal(bool dump_data) const
{
    std::cerr << "    m_d -> c_nr   = " << c_nr << std::endl;
    std::cerr << "    m_d -> c_nc   = " << c_nc << std::endl;
    std::cerr << "    m_d -> c_cnt  = " << c_cnt << std::endl;
    if (dump_data) {
        std::cerr << "    m_d -> c_data = ";
        for (int i = 0; i < (c_nr * c_nc); ++i)
            std::cerr << c_data[i] << " ";
        std::cerr << std::endl;
    }
}



// ------------------------- MatrixBaseRef implementation ------------------------- //

template <typename T>
inline MatrixBaseRef<T>::MatrixBaseRef(MatrixBase<T> & M, const int i, const int j) : m_ref(M), m_i(i), m_j(j)
{
}


template <typename T>
inline MatrixBaseRef<T> & MatrixBaseRef<T>::operator=(T val)
{
    // COPY-ON-WRITE: detach the container if 'reference counter' > 1.
    MatrixBase<T>::container::detach(m_ref.m_d);

    // Modify the element in the (i,j) position (previously stored).
    // Note indexes are verified in the MatrixBase::operator()(i,j).
    m_ref.m_d->set(m_i, m_j, val);

    // Return the reference to the MatrixBaseRef object.
    return *this;
}


template <typename T>
inline MatrixBaseRef<T>::operator T()
{
    // Return the element in the (i,j) position (previously stored).
    // Note indexes are verified in the MatrixBase::operator()(i,j).
    return m_ref.m_d->get(m_i, m_j);
}



// ------------------ MatrixBase implementation (normal members) ------------------ //

template <typename T>
inline MatrixBase<T>::MatrixBase() noexcept : m_d(nullptr)
{
}


template<typename T>
inline MatrixBase<T>::~MatrixBase() noexcept
{
    container::destroy(m_d);
}


template <typename T>
inline MatrixBase<T>::MatrixBase(const int nrows, const int ncols, const bool init) : m_d(nullptr)
{
    MATRIXBASE_ASSERT(nrows > 0 && ncols > 0 && nrows <= max_nrows && ncols <= max_ncols, "nrows,ncols", "size error");

    m_d = container::alloc(nrows, ncols, init);
}


template <typename T>
inline MatrixBase<T>::MatrixBase(const int nrows, const int ncols, const std::initializer_list<T> & list) : m_d(nullptr)
{
    MATRIXBASE_ASSERT(nrows > 0 && ncols > 0 && nrows <= max_nrows && ncols <= max_ncols, "nrows,ncols,list<>", "size error");
    MATRIXBASE_ASSERT(static_cast<int>(list.size()) == nrows*ncols, "nrows,ncols,list<>", "list size error");

    m_d = container::alloc(nrows, ncols, false);

    auto p = list.begin();
    for (int i = 0; i < nrows; ++i)
        for(int j = 0; j < ncols; ++j)
            m_d->set(i,j, *p++);
}


template <typename T>
inline MatrixBase<T>::MatrixBase(const std::initializer_list< std::initializer_list<T>> & list) : m_d(nullptr)
{
    // Get the number of initializer lists (number of matrix rows) and
    // the size of the first initializer list (number of matrix columns).
    int nrows = static_cast<int>(list.size());
    int ncols = static_cast<int>(list.begin()->size());

    // Verify that all lists (matrix rows) have the same
    // number of elements (i.e. number of matrix columns).
    for (auto const & row : list)
        MATRIXBASE_ASSERT(static_cast<int>(row.size()) == ncols, "list<list<>>", "list size error");

    // Alloc the container.
    m_d = container::alloc(nrows, ncols, false);

    // Copy data from the initializer lists.
    int i = 0;
    for (auto const & row : list) {
        int j = 0;
        for(auto const & col : row) {
            m_d->set(i,j, col);
            ++j;
        }
        ++i;
    }
}


template <typename T>
inline  MatrixBase<T>::MatrixBase(const int nrows, const int ncols, T * array) : m_d(nullptr)
{
    MATRIXBASE_ASSERT(array, "nrows,ncols,array", "null pointer");
    MATRIXBASE_ASSERT(nrows > 0 && ncols > 0 && nrows <= max_nrows && ncols <= max_ncols, "nrows,ncols,array", "size error");

    m_d = container::alloc(nrows, ncols, false);

    for (int i = 0; i < nrows; ++i)
        for(int j = 0; j < ncols; ++j)
            m_d->set(i,j, *array++);
}


template<typename T>
inline MatrixBase<T>::MatrixBase(const T v) : m_d(nullptr)
{
    m_d = container::alloc(1, 1, false);
    m_d->set(0, v);
}


template<typename T>
inline MatrixBase<T>::MatrixBase(const MatrixBase<T> & M): m_d(nullptr)
{
    m_d = container::copy(M.m_d);
}


template<typename T>
inline MatrixBase<T>::MatrixBase(MatrixBase<T> && M) noexcept : m_d(nullptr)
{
    m_d = container::move(M.m_d);
}


template<typename T>
inline MatrixBase<T> & MatrixBase<T>::operator=(const MatrixBase<T> & M)
{
    if (this != &M) {
        container::destroy(m_d);
        m_d = container::copy(M.m_d);
    }
    return *this;
}


template<typename T>
inline MatrixBase<T> & MatrixBase<T>::operator=(MatrixBase<T> && M) noexcept
{
    if (this != &M) {
        container::destroy(m_d);
        m_d = container::move(M.m_d);
    }
    return *this;
}


template <typename T>
inline MatrixBaseRef<T> MatrixBase<T>::operator()(const int i, const int j)
{
    MATRIXBASE_ASSERT(m_d, "i,j", "null matrix");
    MATRIXBASE_ASSERT(i >= 0 && j >= 0 && i < m_d->nr() && j < m_d->nc(), "i,j", "index out of range");

    return MatrixBaseRef<T>(*this, i, j);           // Crea l'oggetto proxy, salvando il riferimento alla matrice
}                                                   // e la posizione (i,j) dell'elemento a cui si vuole accedere.


template <typename T>
inline T MatrixBase<T>::operator()(const int i, const int j) const
{
    MATRIXBASE_ASSERT(m_d, "i,j", "null matrix");
    MATRIXBASE_ASSERT(i >= 0 && j >= 0 && i < m_d->nr() && j < m_d->nc(), "i,j", "index out of range");

    return m_d->get(i, j);
}


template <typename T>
inline MatrixBase<T> MatrixBase<T>::operator+() const
{
    MATRIXBASE_ASSERT(m_d, "", "null matrix");

    return *this;
}


template <typename T>
inline MatrixBase<T> MatrixBase<T>::operator-() const
{
    MATRIXBASE_ASSERT(m_d, "", "null matrix");

    MatrixBase<T> Tmp(m_d->nr(), m_d->nc(), false);
    for (int k = 0; k < m_d->nr() * m_d->nc(); ++k)
            Tmp.m_d->set(k, -m_d->get(k));

    return Tmp;
}


template <typename T>
inline MatrixBase<T>::operator T() const
{
    MATRIXBASE_ASSERT(m_d, "", "null matrix");
    MATRIXBASE_ASSERT(m_d->nr() == 1 && m_d->nc() == 1, "", "size error");

    return m_d->get(0);
}


template <typename T>
template <typename S, typename>
inline MatrixBase<T>::MatrixBase(const MatrixBase<S> & M) : m_d(nullptr)
{
    MatrixBase<T> Tmp = M.template casted_copy<T>();
    m_d = container::move(Tmp.m_d);
}


template <typename T>
inline bool MatrixBase<T>::isnull() const noexcept
{
    return !m_d;
}


template <typename T>
inline int MatrixBase<T>::nrows() const noexcept
{
    return m_d ? m_d->nr() : 0;
}


template <typename T>
inline int MatrixBase<T>::ncols() const noexcept
{
    return m_d ? m_d->nc() : 0;
}


template <typename T>
inline MatrixBase<T> MatrixBase<T>::tran() const
{
    MATRIXBASE_ASSERT(m_d, "", "null matrix");

    MatrixBase<T> Tmp(m_d->nc(), m_d->nr(), false);
    for(int j = 0; j < m_d->nc(); ++j)
        for (int i = 0; i < m_d->nr(); ++i)
            Tmp.m_d->set(j,i, m_d->get(i,j));

    return Tmp;
}


template <typename T>
inline MatrixBase<T> MatrixBase<T>::conj() const
{
    MATRIXBASE_ASSERT(m_d, "", "null matrix");

    if (!std::is_complex<T>::value)
        return *this;

    MatrixBase<T> Tmp(m_d->nr(), m_d->nc(), false);
    for (int k = 0; k < m_d->nr() * m_d->nc(); ++k)
            Tmp.m_d->set(k, std::conj(m_d->get(k)));

    return Tmp;
}


template <typename T>
inline MatrixBase<T> MatrixBase<T>::ctran() const
{
    MATRIXBASE_ASSERT(m_d, "", "null matrix");

    MatrixBase<T> Tmp(m_d->nc(), m_d->nr(), false);
    for(int j = 0; j < m_d->nc(); ++j)
        for (int i = 0; i < m_d->nr(); ++i)
            Tmp.m_d->set(j,i, std::conj(m_d->get(i,j)));

    return Tmp;
}


template <typename T>
template <typename S, typename>
inline auto MatrixBase<T>::real() const -> MatrixBase<typename get_complex_subtype<S>::type>
{
    MATRIXBASE_ASSERT(m_d, "", "null matrix");

    MatrixBase<typename get_complex_subtype<S>::type> Tmp(m_d->nr(), m_d->nc(), false);
    for (int k = 0; k < m_d->nr() * m_d->nc(); ++k)
            Tmp.m_d->set(k, std::real(m_d->get(k)));

    return Tmp;
}


template <typename T>
template <typename S, typename>
inline auto MatrixBase<T>::imag() const -> MatrixBase<typename get_complex_subtype<S>::type>
{
    MATRIXBASE_ASSERT(m_d, "", "null matrix");

    MatrixBase<typename get_complex_subtype<S>::type> Tmp(m_d->nr(), m_d->nc(), false);
    for (int k = 0; k < m_d->nr() * m_d->nc(); ++k)
            Tmp.m_d->set(k, std::imag(m_d->get(k)));

    return Tmp;
}


template <typename T>
inline MatrixBase<T> MatrixBase<T>::submat(int r1, int c1, int r2, int c2) const
{
    MATRIXBASE_ASSERT(m_d, "", "null matrix");
    MATRIXBASE_ASSERT(r1 >= 0 && c1 >= 0 && r1 < m_d->nr() && c1 < m_d->nc(), "r1,c1,r2,c2", "index out of range");
    MATRIXBASE_ASSERT(r2 >= 0 && c2 >= 0 && r2 < m_d->nr() && c2 < m_d->nc(), "r1,c1,r2,c2", "index out of range");

    if (r1 > r2) {
        int tmp = r1; r1 = r2; r2 = tmp;
    }
    if (c1 > c2) {
        int tmp = c1; c1 = c2; c2 = tmp;
    }
    const int nrows = r2 - r1 + 1;
    const int ncols = c2 - c1 + 1;

    if (nrows == m_d->nr() && ncols == m_d->nc())
        return *this;

    MatrixBase<T> Tmp(nrows, ncols, false);
    for(int j = 0; j < ncols; ++j)
        for (int i = 0; i < nrows; ++i)
            Tmp.m_d->set(i,j, m_d->get(r1+i, c1+j));

    return Tmp;
}


template <typename T>
inline void MatrixBase<T>::resize(const int nrows, const int ncols, const bool init)
{
    // NOTA: here the indexes can be BOTH 0.
    MATRIXBASE_ASSERT((nrows == 0 && ncols == 0) || (nrows != 0 && ncols != 0), "nrows,ncols", "size error");
    MATRIXBASE_ASSERT(nrows >= 0 && ncols >= 0 && nrows <= max_nrows && ncols <= max_ncols, "nrows,ncols", "size error");

    // If the current matrix is null, allocate the container (if necessary).
    if (!m_d) {
        if (nrows > 0 && ncols > 0)
            m_d = container::alloc(nrows, ncols, init);
        return;
    }

    // The current matrix is not null, if the new indexes are 0 make the matrix null.
    if (nrows == 0 && ncols == 0){
        container::destroy(m_d);
        return;
    }

    // New indexes are not 0, if they are equal to current indexes, do nothing.
    if (nrows == m_d->nr() && ncols == m_d->nc())
        return;

    // Allocate the new container, possibly initializing to 0 the
    // new elements if the matrix has been enlarged (if it has been
    // reduced all elements will be overwritten by the copy of values).
    container * m_p = container::alloc(nrows, ncols, (nrows > m_d->nr() || ncols > m_d->nc()) ? init : false);

    // Copy the elements (column major) from the old container to the new one.
    for (int j = 0; j < std::min(m_p->nc(), m_d->nc()); ++j)
        for (int i = 0; i < std::min(m_p->nr(), m_d->nr()); ++i)
            m_p->set(i,j, m_d->get(i,j));

    // Free the old container and assing to the matrix the new one.
    container::destroy(m_d);
    m_d = m_p;
}


template <typename T>
inline int MatrixBase<T>::nr_unsafe() const noexcept
{
    return m_d->nr();
}


template <typename T>
inline int MatrixBase<T>::nc_unsafe() const noexcept
{
    return m_d->nc();
}


template <typename T>
inline int MatrixBase<T>::cnt_unsafe() const noexcept
{
    return m_d->cnt();
}


template <typename T>
inline T MatrixBase<T>::get_unsafe(const int k) const noexcept
{
    return m_d->get(k);
}


template <typename T>
inline T MatrixBase<T>::get_unsafe(const int i, const int j) const noexcept
{
    return m_d->get(i,j);
}


template <typename T>
inline void MatrixBase<T>::set_unsafe(const int k, const T v) noexcept
{
    m_d->set(k,v);
}


template <typename T>
inline void MatrixBase<T>::set_unsafe(const int i, const int j, const T v) noexcept
{
    m_d->set(i,j,v);
}


template <typename T>
inline T & MatrixBase<T>::ref_unsafe(const int i, const int j) noexcept
{
    return m_d->ref(i,j);
}


template <typename T>
inline const int *MatrixBase<T>::blas_ref_nr() const noexcept
{
    return & m_d->ref_nr();
}


template <typename T>
inline const int *MatrixBase<T>::blas_ref_nc() const noexcept
{
    return & m_d->ref_nc();
}


template <typename T>
inline const T *MatrixBase<T>::blas_ref_ma() const noexcept
{
    return & m_d->ref(0,0);
}


template <typename T>
template <typename S>
inline MatrixBase<S> MatrixBase<T>::casted_copy() const
{
    MatrixBase<S> Tmp;

    if (m_d) {
        Tmp.resize(m_d->nr(), m_d->nc(), false);
        for (int k = 0; k < m_d->nr() * m_d->nc(); ++k)
                Tmp.m_d->set(k, static_cast<S>(m_d->get(k)));
    }

    return Tmp;
}


template <typename T>
inline void MatrixBase<T>::dump_internal(const char *name, bool dump_data) const
{
    std::cerr << "Dump of " << boost::typeindex::type_id<MatrixBase<T>>().pretty_name() << " " << name << ":" << std::endl;
    std::cerr << "    m_d = " << m_d << std::endl;
    if (m_d)
        m_d -> dump_internal(dump_data);
}



// -------------------------------- Ostream output -------------------------------- //

#include <vector>
#include <sstream>

template <typename T>
inline void MatrixBase<T>::print(const char *preamble, std::ios_base &(*iomanip)(std::ios_base &), const int fp_prec) const
{
    print(std::cout, preamble, iomanip, fp_prec);
}


template <typename T>
void MatrixBase<T>::print(std::ostream & out, const char *preamble, std::ios_base &(*iomanip)(std::ios_base &), const int fp_prec) const
{
    MATRIXBASE_ASSERT(iomanip == std::fixed || iomanip == std::scientific || iomanip == std::defaultfloat, "", "invalid ios manipolator");
    MATRIXBASE_ASSERT(fp_prec >= 0 && fp_prec <= 15, "", "invalid precision");

    // Check if matrix is null.
    if (!m_d) {
        out << preamble << "[]\n";
        return;
    }

    // Save state of output object. See:
    // https://stackoverflow.com/questions/2273330
    std::ios ios_state(nullptr);
    ios_state.copyfmt(out);

    // Get left allineament.
    const int left_sp = strlen(preamble);

    // Store matrix column sizes for better output.
    std::vector<int> col_size_real;
    std::vector<int> col_size_imag;

    // Calculate best size for each matrix column.
    for (int j = 0; j < m_d->nc(); ++j)
    {
        size_t max_size_real = 0;
        size_t max_size_imag = 0;
        for(int i = 0; i < m_d->nr(); ++i)
        {
            if (std::is_complex<T>::value) {
                std::stringstream ss_real;
                std::stringstream ss_imag;
                if (std::is_complex_floating_point<T>::value) {
                    ss_real << (*iomanip) << std::setprecision(fp_prec);
                    ss_imag << (*iomanip) << std::setprecision(fp_prec);
                }
                ss_real << std::real(m_d->get(i,j));
                ss_imag << std::imag(m_d->get(i,j));
                max_size_real = std::max(max_size_real, ss_real.str().length());
                max_size_imag = std::max(max_size_imag, ss_imag.str().length());
            } else{
                std::stringstream ss_real;
                if (std::is_floating_point<T>::value)
                    ss_real << (*iomanip) << std::setprecision(fp_prec);
                ss_real << m_d->get(i,j);
                max_size_real = std::max(max_size_real, ss_real.str().length());
            }
        }
        col_size_real.push_back(static_cast<int>(max_size_real));
        if (std::is_complex<T>::value)
            col_size_imag.push_back(static_cast<int>(max_size_imag));
    }

    // Set output format for floating point types.
    if (std::is_floating_point<T>::value || std::is_complex_floating_point<T>::value)
        out << (*iomanip) << std::setprecision(fp_prec);

    // Display matrix elements.
    for(int i = 0; i < m_d->nr(); ++i)
    {
        if (i == 0)
            out << preamble;
        else
            out << std::setw(left_sp + 1);
        out << "[";
        for (int j = 0; j < m_d->nc(); ++j)
        {
            if (std::is_complex<T>::value) {
                out << "("
                    << std::setw(col_size_real[j]) << std::real(m_d->get(i,j))
                    << ","
                    << std::setw(col_size_imag[j]) << std::imag(m_d->get(i,j))
                    << ")";
            } else
                out << std::setw(col_size_real[j]) << m_d->get(i,j);
            if (j < m_d->nc() -1)
                out << "  ";
        }
        out << "]" << std::endl;
    }

    // Restore state of output object.
    out.copyfmt(ios_state);
}



// ------------------ MatrixBase implementation (static members) ------------------ //

#include <chrono>                   // Use std::chrono::system_clock
#include <random>                   // Use pseudocasual generators


template <typename T>
inline MatrixBase<T> MatrixBase<T>::eye(const int n)
{
    MATRIXBASE_ASSERT(n > 0 && n <= max_nrows && n <= max_ncols, "n", "size error");

    MatrixBase<T> Tmp(n, n);
    for (int i = 0; i < n; ++i)
        Tmp.m_d->set(i,i, 1);

    return Tmp;
}


template <typename T>
inline MatrixBase<T> MatrixBase<T>::ones(const int n)
{
    return MatrixBase<T>::ones(n, n);
}


template <typename T>
inline MatrixBase<T> MatrixBase<T>::ones(const int m, const int n)
{
    MATRIXBASE_ASSERT(m > 0 && n > 0 && m <= max_nrows && n <= max_ncols, "m,n", "size error");

    MatrixBase<T> Tmp(m, n, false);
    for (int k = 0; k < m*n; ++k)
            Tmp.m_d->set(k, 1);

    return Tmp;
}


template <typename T>
inline MatrixBase<T> MatrixBase<T>::zeros(const int n)
{
    return MatrixBase<T>::zeros(n, n);
}


template <typename T>
inline MatrixBase<T> MatrixBase<T>::zeros(const int m, const int n)
{
    MATRIXBASE_ASSERT(m > 0 && n > 0 && m <= max_nrows && n <= max_ncols, "m,n", "size error");

    MatrixBase<T> Tmp(m, n);
    return Tmp;
}


template <typename T> template <typename S>
inline MatrixBase<T> MatrixBase<T>::create_random_matrix(S & dist, const int m, const int n)
{
    // Get the system ticks from the epoc.
    auto seed1 = std::chrono::system_clock::now().time_since_epoch().count();

    // Uniformly-distributed random number generator that may access a hardware device
    // in your system, or something like /dev/random on Linux. It is usually used only
    // to seed a pseudo-random generator like mt19937, since the underlying device will
    // usually run out of entropy quickly. Also it is slow and does not work correctly on
    // all platforms, i.e. it might return always the same number at each application start
    // (because it falls back to a PRNG if a true random hardware source is not available).
    std::random_device rd;
    auto seed2 = rd();

    // We can combine the two seed values to get more randomness. We could also add other
    // sources of randomness if we have them or provide more randome_device values if really
    // hardware randomized. Also we could add a static counter if several generator must be
    // seeded in different threads (in this case the clock does not ensure different values).
    std::seed_seq seed{ static_cast<uint32_t>(seed1), static_cast<uint32_t>(seed2) };

    // Fast pseudo-random number generator using the Mersenne Twister engine which,
    // according to the original authors' paper title, is also uniform. This generates
    // fully random 32-bit (mt19937) or 64-bit (mt19937_64) unsigned integers. The seed
    // of this generator does not have to be uniform itself (we can use a timestamp or
    // any other value). Designed for simulations and games, it is not suitable for
    // cryptographics applications where lot of random values are needed, because a
    // long sequence of generated number permit to predict the mt19937 internal state.
    std::mt19937_64 rng(seed);

    // Fill the matrix with random values obtained from the above mt19937_64 generator.
    MatrixBase<T> Tmp(m, n, false);
    for (int k = 0; k < m*n; ++k)
            Tmp.m_d->set(k, dist(rng));

    return Tmp;
}


template <typename T>
inline MatrixBase<T> MatrixBase<T>::rand(const int n, const T lower, const T upper)
{
    return MatrixBase<T>::rand(n, n, lower, upper);
}


template <typename T>
inline MatrixBase<T> MatrixBase<T>::rand(const int m, const int n, const T lower, const T upper)
{
    MATRIXBASE_ASSERT(m > 0 && n > 0 && m <= max_nrows && n <= max_ncols, "m,n", "size error");

    // NOTE: ONLY FLOAT, DOUBLE, LONG DOUBLE.
    std::uniform_real_distribution<T> dist(lower, upper);   // Uniform distribution.
    return create_random_matrix(dist, m, n);
}


template <typename T>
inline MatrixBase<T> MatrixBase<T>::randn(const int n, const T mean, const T devstd)
{
    return MatrixBase<T>::randn(n, n, mean, devstd);
}


template <typename T>
inline MatrixBase<T> MatrixBase<T>::randn(const int m, const int n, const T mean, const T devstd)
{
    MATRIXBASE_ASSERT(m > 0 && n > 0 && m <= max_nrows && n <= max_ncols, "m,n", "size error");

    // NOTE: ONLY FLOAT, DOUBLE, LONG DOUBLE.
    std::normal_distribution<T> dist(mean, devstd);         // Normal distribution.
    return create_random_matrix(dist, m, n);
}


template <typename T>
inline MatrixBase<T> MatrixBase<T>::hilb(const int n)
{
    MATRIXBASE_ASSERT(n > 0 && n <= max_nrows && n <= max_ncols, "n", "size error");

    MatrixBase<T> Tmp(n, n, false);

    for(int j = 0; j < n; ++j)
        for (int i = 0; i < n; ++i)
            Tmp.m_d->set(i,j, T(1) / ((i+1) + (j+1) - 1));

    return Tmp;
}


// //////////////////////////////////////////////////////////////////////////////// //
//                          Other functions and operators                           //
// //////////////////////////////////////////////////////////////////////////////// //

#include <limits>       // Use std::numeric_limits<T>::epsilon()
#include <cassert>      // Use assert()


// Integer matrix comparison (same type only).

template <typename T, typename = std::enable_if_t<std::is_integral<T>::value>>
inline bool operator==(const MatrixBase<T> & A, const MatrixBase<T> & B)
{
    MATRIX_BINARY_OPMM_ASSERT(!A.isnull() && !B.isnull(), "null matrix");
    MATRIX_BINARY_OPMM_ASSERT(A.nr_unsafe() == B.nr_unsafe() && A.nc_unsafe() == B.nc_unsafe(), "sizes differ");

    for (int k = 0; k < A.nr_unsafe() * A.nc_unsafe(); ++k)
        if (A.get_unsafe(k) != B.get_unsafe(k))
            return false;
    return true;
}


template <typename T, typename = std::enable_if_t<std::is_integral<T>::value>>
inline bool operator!=(const MatrixBase<T> & A, const MatrixBase<T> & B)
{
    return !(A == B);
}


// Floating point matrix comparison (same type only).
//
// Operator == cannot be used with floating point values: due to approximations, errors
// propagation and rounds in math operations, we must verify if two floating point values
// are within a well established range. The problem is: which is the range to consider?
// We might, at first, consider Epsilon(), that is the smallest value that could be added
// to 1.0 to change its value, but floating point values are not equidistant! For numbers
// smaller than 1.0, epsilon() quickly becomes too large to be useful, while for numbers
// greater than 2.0, epsilon() become smaller than the distance between adjacent values.
// For such reasons we need to perform a weighted compare. Also we must ignore some of the
// least significant bits to take in account the noise of previous calculations. A good
// formula for floating point comparison is abs(a-b) <= k * eps * max(1,abs(a),abs(b))
// where k is an integer magnification factor for epsilon(), provided by user. See:
// - https://bitbashing.io/comparing-floats.html
// - http://realtimecollisiondetection.net/blog/?p=89

template <typename T>
inline bool matrix_helper_almost_equal(const T a, const T b, const int k)
{
    if (std::isnan(a) || std::isnan(b) || std::isinf(a) || std::isinf(b))
        return false;

    const T toll =  k * std::numeric_limits<T>::epsilon();
    return std::fabs(a - b) <= toll * std::fmax(T(1), std::fmax(std::fabs(a), std::fabs(b)));
}


template <typename T>
inline bool matrix_helper_almost_equal(const std::complex<T> a, const std::complex<T> b, const int k)
{
    return matrix_helper_almost_equal(std::real(a), std::real(b), k) && matrix_helper_almost_equal(std::imag(a), std::imag(b), k);
}


template <typename T, typename = std::enable_if_t<std::is_floating_point<T>::value || std::is_complex_floating_point<T>::value>>
inline bool almost_equal(const MatrixBase<T> & A, const MatrixBase<T> & B, const int eps_mag = 32)
{
    MATRIX_BINARY_OPMM_ASSERT(!A.isnull() && !B.isnull(), "null matrix");
    MATRIX_BINARY_OPMM_ASSERT(A.nr_unsafe() == B.nr_unsafe() && A.nc_unsafe() == B.nc_unsafe(), "sizes differ");

    for (int k = 0; k < A.nr_unsafe() * A.nc_unsafe(); ++k)
        if (!matrix_helper_almost_equal(A.get_unsafe(k), B.get_unsafe(k), eps_mag))
            return false;
    return true;
}


// Math operators
//
// NOTE: versions with rvalue references are not currently implemented because
// they require that the function arguments will be compatible with the in-place
// modification of the matrix. For example, it is not possible to sum complex items
// to an already allocated temporary real matrix without a new allocation and a copy,
// but this will make the use of rvalue references completely useless. The solution is
// to instantiate the function (or the operator) only when the rvalue type is compatible
// with the in-place modification of the matrix (it depends also on the type of the other
// parameter). We can impose such condition using "SFINAE" and "std::enable_if_t" type
// trait. I will implement this in the future, for the moment I can live without it!
// NOTE: SFINAE is required to istanziate the operators only on the right types.

// Sum between a matrix and a scalar value or between two matrices (element by element).

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<T>::value || std::is_complex<T>::value>>
inline auto operator+(const MatrixBase<S> & M, const T v) -> MatrixBase<decltype(S()+T())>
{
    MATRIX_BINARY_OPMV_ASSERT(!M.isnull(), "null matrix");

    MatrixBase<decltype(S()+T())> Tmp(M.nr_unsafe(), M.nc_unsafe(), false);
    for (int k = 0; k < M.nr_unsafe() * M.nc_unsafe(); ++k)
        Tmp.set_unsafe(k, M.get_unsafe(k) + v);

    return Tmp;
}


template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value || std::is_complex<S>::value>>
inline auto operator+(const S v, const MatrixBase<T> & M) -> MatrixBase<decltype(S()+T())>
{
    MATRIX_BINARY_OPVM_ASSERT(!M.isnull(), "null matrix");

    MatrixBase<decltype(S()+T())> Tmp(M.nr_unsafe(), M.nc_unsafe(), false);
    for (int k = 0; k < M.nr_unsafe() * M.nc_unsafe(); ++k)
        Tmp.set_unsafe(k, v + M.get_unsafe(k));

    return Tmp;
}


template <typename S, typename T>
inline auto operator+(const MatrixBase<S> & A, const MatrixBase<T> & B) -> MatrixBase<decltype(S()+T())>
{
    MATRIX_BINARY_OPMM_ASSERT(!A.isnull() && !B.isnull(), "null matrix");
    MATRIX_BINARY_OPMM_ASSERT(A.nr_unsafe() == B.nr_unsafe() && A.nc_unsafe() == B.nc_unsafe(), "sizes differ");

    MatrixBase<decltype(S()+T())> Tmp(A.nr_unsafe(), A.nc_unsafe(), false);
    for (int k = 0; k < A.nr_unsafe() * A.nc_unsafe(); ++k)
        Tmp.set_unsafe(k, A.get_unsafe(k) + B.get_unsafe(k));

    return Tmp;
}


// Subtraction between a matrix and a scalar value or between two matrices (element by element).

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<T>::value || std::is_complex<T>::value>>
inline auto operator-(const MatrixBase<S> & M, const T v) -> MatrixBase<decltype(S()+T())>
{
    MATRIX_BINARY_OPMV_ASSERT(!M.isnull(), "null matrix");

    MatrixBase<decltype(S()+T())> Tmp(M.nr_unsafe(), M.nc_unsafe(), false);
    for (int k = 0; k < M.nr_unsafe() * M.nc_unsafe(); ++k)
        Tmp.set_unsafe(k, M.get_unsafe(k) - v);

    return Tmp;
}


template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value || std::is_complex<S>::value>>
inline auto operator-(const S v, const MatrixBase<T> & M) -> MatrixBase<decltype(S()+T())>
{
    MATRIX_BINARY_OPVM_ASSERT(!M.isnull(), "null matrix");

    MatrixBase<decltype(S()+T())> Tmp(M.nr_unsafe(), M.nc_unsafe(), false);
    for (int k = 0; k < M.nr_unsafe() * M.nc_unsafe(); ++k)
        Tmp.set_unsafe(k, v - M.get_unsafe(k));

    return Tmp;
}


template <typename S, typename T>
inline auto operator-(const MatrixBase<S> & A, const MatrixBase<T> & B) -> MatrixBase<decltype(S()+T())>
{
    MATRIX_BINARY_OPMM_ASSERT(!A.isnull() && !B.isnull(), "null matrix");
    MATRIX_BINARY_OPMM_ASSERT(A.nr_unsafe() == B.nr_unsafe() && A.nc_unsafe() == B.nc_unsafe(), "sizes differ");

    MatrixBase<decltype(S()+T())> Tmp(A.nr_unsafe(), A.nc_unsafe(), false);
    for (int k = 0; k < A.nr_unsafe() * A.nc_unsafe(); ++k)
        Tmp.set_unsafe(k, A.get_unsafe(k) - B.get_unsafe(k));

    return Tmp;
}


// Product between a matrix and a scalar value or between two matrices (element by element).

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<T>::value || std::is_complex<T>::value>>
inline auto array_mul(const MatrixBase<S> & M, const T v) -> MatrixBase<decltype(S()+T())>
{
    MATRIX_BINARY_OPMV_ASSERT(!M.isnull(), "null matrix");

    MatrixBase<decltype(S()+T())> Tmp(M.nr_unsafe(), M.nc_unsafe(), false);
    for (int k = 0; k < M.nr_unsafe() * M.nc_unsafe(); ++k)
        Tmp.set_unsafe(k, M.get_unsafe(k) * v);

    return Tmp;
}


template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value || std::is_complex<S>::value>>
inline auto array_mul(const S v, const MatrixBase<T> & M) -> MatrixBase<decltype(S()+T())>
{
    MATRIX_BINARY_OPVM_ASSERT(!M.isnull(), "null matrix");

    MatrixBase<decltype(S()+T())> Tmp(M.nr_unsafe(), M.nc_unsafe(), false);
    for (int k = 0; k < M.nr_unsafe() * M.nc_unsafe(); ++k)
        Tmp.set_unsafe(k, v * M.get_unsafe(k));

    return Tmp;
}


template <typename S, typename T>
inline auto array_mul(const MatrixBase<S> & A, const MatrixBase<T> & B) -> MatrixBase<decltype(S()+T())>
{
    MATRIX_BINARY_OPMM_ASSERT(!A.isnull() && !B.isnull(), "null matrix");
    MATRIX_BINARY_OPMM_ASSERT(A.nr_unsafe() == B.nr_unsafe() && A.nc_unsafe() == B.nc_unsafe(), "sizes differ");

    MatrixBase<decltype(S()+T())> Tmp(A.nr_unsafe(), A.nc_unsafe(), false);
    for (int k = 0; k < A.nr_unsafe() * A.nc_unsafe(); ++k)
        Tmp.set_unsafe(k, A.get_unsafe(k) * B.get_unsafe(k));

    return Tmp;
}


template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<T>::value || std::is_complex<T>::value>>
inline auto operator*(const MatrixBase<S> & M, const T v) -> MatrixBase<decltype(S()+T())>
{
    return array_mul(M,v);
}


template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value || std::is_complex<S>::value>>
inline auto operator*(const S v, const MatrixBase<T> & M) -> MatrixBase<decltype(S()+T())>
{
    return array_mul(v,M);
}


// Division between a matrix and a scalar value or between two matrices (element by element).

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<T>::value || std::is_complex<T>::value>>
inline auto array_div(const MatrixBase<S> & M, const T v) -> MatrixBase<decltype(S()+T())>
{
    MATRIX_BINARY_OPMV_ASSERT(!M.isnull(), "null matrix");

    MatrixBase<decltype(S()+T())> Tmp(M.nr_unsafe(), M.nc_unsafe(), false);
    for (int k = 0; k < M.nr_unsafe() * M.nc_unsafe(); ++k)
        Tmp.set_unsafe(k, M.get_unsafe(k) / v);

    return Tmp;
}


template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value || std::is_complex<S>::value>>
inline auto array_div(const S v, const MatrixBase<T> & M) -> MatrixBase<decltype(S()+T())>
{
    MATRIX_BINARY_OPVM_ASSERT(!M.isnull(), "null matrix");

    MatrixBase<decltype(S()+T())> Tmp(M.nr_unsafe(), M.nc_unsafe(), false);
    for (int k = 0; k < M.nr_unsafe() * M.nc_unsafe(); ++k)
        Tmp.set_unsafe(k, v / M.get_unsafe(k));

    return Tmp;
}


template <typename S, typename T>
inline auto array_div(const MatrixBase<S> & A, const MatrixBase<T> & B) -> MatrixBase<decltype(S()+T())>
{
    MATRIX_BINARY_OPMM_ASSERT(!A.isnull() && !B.isnull(), "null matrix");
    MATRIX_BINARY_OPMM_ASSERT(A.nr_unsafe() == B.nr_unsafe() && A.nc_unsafe() == B.nc_unsafe(), "sizes differ");

    MatrixBase<decltype(S()+T())> Tmp(A.nr_unsafe(), A.nc_unsafe(), false);
    for (int k = 0; k < A.nr_unsafe() * A.nc_unsafe(); ++k)
        Tmp.set_unsafe(k, A.get_unsafe(k) / B.get_unsafe(k));

    return Tmp;
}


template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<T>::value || std::is_complex<T>::value>>
inline auto operator/(const MatrixBase<S> & M, const T v) -> MatrixBase<decltype(S()+T())>
{
    return array_div(M,v);
}


// Matrix product rows by columns (see also the optimized version in "matrix_blas.hxx").
// NOTE:  this function is used only when the GEMM BLAS optimized version cannot be used
// (for example when the operator is invoked with types not supported by BLAS, or when the
// two matrices have different types for which the GEMM BLAS subroutine is not applicable).
// NOTE: BLAS supports only "float", "double", "complex<float>" and "complex<double>" types.

template <typename S, typename T>
inline auto operator*(const MatrixBase<S> & A, const MatrixBase<T> & B) -> MatrixBase<decltype(S()+T())>
{
    MATRIX_BINARY_OPMM_ASSERT(!A.isnull() && !B.isnull(), "null matrix");
    MATRIX_BINARY_OPMM_ASSERT(A.nc_unsafe() == B.nr_unsafe(), "invalid sizes");

    typedef decltype(S()+T()) prod_t;

    // Up to 8x8 matrices calculates the product with the trivial way (more efficient).
    if (A.nr_unsafe() < 8 && A.nc_unsafe() < 8 && B.nc_unsafe() < 8) {
        MatrixBase<prod_t> Tmp(A.nr_unsafe(), B.nc_unsafe(), false);   // Initialization not required.
        for (int j = 0; j < B.nc_unsafe(); ++j) {
            for(int i = 0; i < A.nr_unsafe(); ++i) {
                prod_t sum = 0;
                for (int k = 0; k < A.nc_unsafe(); ++k)
                    sum += A.get_unsafe(i,k) * B.get_unsafe(k,j);
                Tmp.set_unsafe(i,j, sum);
            }
        }
        return Tmp;
    }

    // For matrices over 8x8 apply some optimizations:
    // - split the matrices into blocks to not overload the cache;
    // - each block product is calculated using the "OpenMP parallel for";
    // - in each OpenMP thread the innermost cycle is unrolled on 8 products.
    constexpr int blk_size = 64;
    MatrixBase<prod_t> Tmp(A.nr_unsafe(), B.nc_unsafe(), true);        // Must be initialized.
    for (int jb = 0; jb < B.nc_unsafe(); jb += blk_size)
    {
        int je = std::min(B.nc_unsafe(), jb + blk_size);
        for(int ib = 0; ib < A.nr_unsafe(); ib += blk_size)
        {
            int ie = std::min(A.nr_unsafe(), ib + blk_size);
            for (int kb = 0; kb < A.nc_unsafe(); kb += blk_size)
            {
                int ke = std::min(A.nc_unsafe(), kb + blk_size);
                int k8 = (ke >> 3) << 3;

                #pragma omp parallel for
                for (int j = jb; j < je; ++j) {
                    for(int i = ib; i < ie; ++i) {

                        int k;
                        prod_t s[9];
                        s[0] = s[1] = s[2] = s[3] = s[4] = s[5] = s[6] = s[7] = s[8] = 0;

                        for (k = kb; k < k8; k += 8) {
                            s[0] += A.get_unsafe(i,k+0) * B.get_unsafe(k+0,j);
                            s[1] += A.get_unsafe(i,k+1) * B.get_unsafe(k+1,j);
                            s[2] += A.get_unsafe(i,k+2) * B.get_unsafe(k+2,j);
                            s[3] += A.get_unsafe(i,k+3) * B.get_unsafe(k+3,j);
                            s[4] += A.get_unsafe(i,k+4) * B.get_unsafe(k+4,j);
                            s[5] += A.get_unsafe(i,k+5) * B.get_unsafe(k+5,j);
                            s[6] += A.get_unsafe(i,k+6) * B.get_unsafe(k+6,j);
                            s[7] += A.get_unsafe(i,k+7) * B.get_unsafe(k+7,j);
                        }

                        for (; k < ke; ++k)
                            s[8] += A.get_unsafe(i,k) * B.get_unsafe(k,j);
                        Tmp.ref_unsafe(i,j) += s[0] + s[1] + s[2] + s[3] + s[4] + s[5] + s[6] + s[7] + s[8];
                    }
                }
            }
        }
    }
    return Tmp;
}


// Function helper, used by matrix_helper_getf2(). Find the
// index of element into the array having max absolute value.

template <typename T>
inline int matrix_helper_idxmax(const int n, const T *dx)
{
    if (n == 1) {
        return 0;
    }

    int idx_max = 0;
    decltype(std::abs(T())) vmax = std::abs(dx[0]);

    for (int i = 1; i < n; ++i)
    {
        if (std::abs(dx[i]) > vmax) {
            idx_max = i;
            vmax = std::abs(dx[i]);
        }
    }
    return idx_max;
}


// Function helper, used by matrix_helper_getf2(). Performs
// the operation A := A - x*y' where x is an "m" element vector,
// y is an "n" element vector and A is an "m by n" matrix.

template <typename T>
inline void matrix_helper_ger(const int m, const int n, const T *x, const T *y, const int lda, T *a)
{
    if (m == 0 || n == 0)
        return;

    int jy = 0;
    for (int j = 0; j < n; ++j)
    {
        if (y[jy] != T(0)) {
            for (int i = 0; i < m; ++i)
                a[i + j*lda] -= x[i] * y[jy];
        }
        jy += lda;
    }
}


// Function helper, used by matrix_helper_getrf(). Computes an LU factorization
// of a general "m by n" matrix A, using partial pivoting with row interchanges.
// The factorization has the form P*A = L*U where P is a permutation matrix, L is
// a lower triangular matrix with unit diagonal elements (lower trapezoidal if m > n)
// and U is an upper triangular matrix (upper trapezoidal if m < n).

template <typename T>
inline void matrix_helper_getf2(const int m, const int n, T *a, int *ipiv, int *info)
{
    *info = 0;
    for (int j = 0; j < std::min(m,n); ++j)
    {
        int jp = j + matrix_helper_idxmax(m - j, &a[(j) + (j)*m]);  // Find pivot and test for singularity.
        ipiv[j] = jp + 1;                                           // Save pivot: +1 is required to make
                                                                    // "ipiv" compatible with LAPACK.
        if (a[jp + j*m] != T(0))
        {
            if (jp != j)                                            // Apply the interchange to columns 1:N.
           {
                int i, idx = 0;                                     // Swap &a[j] with &a[jp].
                for (i = 0; i < n; ++i)
                {
                    T tmp = a[j + idx];
                    a[j + idx] = a[jp + idx];
                    a[jp + idx] = tmp;
                    idx += m;
                }
            }

            if (j < m - 1)                                          // Compute elements J+1:M of J-th column.
                for (int i = 0; i < (m-(j+1)); ++i)                 // Scales vector by a constant.
                    a[j+1 + j*m + i] *= T(1) / a[j + j*m];

        } else if (*info == 0)
            *info = j+1;                                            // Store singolar column.

        if (j < std::min(m, n))                                     // Update the trailing submatrix.
            matrix_helper_ger((m-(j+1)), (n-(j+1)), &a[j+1 + j*m], &a[j + (j+1)*m], m, &a[j+1 + (j+1)*m]);
    }
}


// LU factorization helper (generic type, not optimized). Here, the matrix A can
// be MxN. NOTE: the matrix A is modified but not detached here. This function is
// used only when the GETRF LAPACK optimized version cannot be used (i.e. when
// the function is invoked with types not supported by LAPACK). See also:
//  - http://matlab.izmiran.ru/help/techdoc/ref/lu.html
//  - http://icl.cs.utk.edu/lapack-forum/viewtopic.php?f=2&t=874

template <typename T>
inline int matrix_helper_getrf(MatrixBase<T> & A, int *ipiv)
{
    assert(ipiv);
    assert(!A.isnull());
    assert(A.cnt_unsafe() == 1);

    int info;
    matrix_helper_getf2(A.nr_unsafe(), A.nc_unsafe(), const_cast<T*>(A.blas_ref_ma()), ipiv, &info);
    return info;
}


// Function helper, used by matrix_helper_getrs(). Interchange
// row 'i' with row 'ipiv[i]-1' for each rows k1...k2. Note that
// 'ipiv' has elements in range 1..n to be compatible with LAPACK.

template <typename T>
inline void matrix_helper_laswp(const int a_nr, const int a_nc, T *a, const int k1, const int k2, const int *ipiv)
{
    for (int i = k1; i <= k2; ++i)
    {
        int ip = ipiv[i] - 1;
        if (ip != i)
        {
            int j, idx = 0;                     // Swap &a[i] with &a[ip].
            for (j = 0; j < a_nc; ++j)
            {
                T tmp = a[i + idx];
                a[i + idx] = a[ip + idx];
                a[ip + idx] = tmp;
                idx += a_nr;
            }
        }
    }
}


// Function helper, used by matrix_helper_getrs().
// Solve an upper triangular or lower triangular system.

template <typename S, typename T>
void matrix_helper_trsm(const bool upper, const bool nounit, const int b_nr, const int b_nc, const S *a, const int lda, T *b)
{
    if (upper) {
        for (int j = 0; j < b_nc; ++j)
        {
            for (int k = b_nr-1; k >= 0; --k)
            {
                if (b[k + j*lda] != T(0)) {
                    if (nounit)
                        b[k + j*lda] = b[k + j*lda] / a[k + k*lda];

                    for (int i = 0; i <= k-1; ++i)
                        b[i + j*lda] = b[i + j*lda] - b[k + j*lda] * a[i + k*lda];
                }
            }
        }
    } else {
        for (int j = 0; j < b_nc; ++j)
        {
            for (int k = 0; k < b_nr; ++k)
            {
                if (b[k + j*lda] != T(0)) {
                    if (nounit)
                        b[k + j*lda] = b[k + j*lda] / a[k + k*lda];

                    for (int i = k + 1; i < b_nr; ++i)
                        b[i + j*lda] = b[i + j*lda] - b[k + j*lda] * a[i + k*lda];
                }
            }
        }
    }
}


// Helper that solves a system of linear equations A*X = B using the LU
// factorization computed with DGETRF (LAPACK or mine). Matrix A must be
// NxN. NOTE that the vector/matrix B is modified but not detached here.
// This function is used only when the GETRS LAPACK optimized version
// cannot be used (for example when the function is invoked with types
// not supported by LAPACK, or when the two matrices have different
// types for which the GETRS LAPACK subroutine is not applicable.

template <typename S, typename T>
inline int matrix_helper_getrs(const MatrixBase<S> & A, MatrixBase<T> & B, const int *ipiv)
{
    assert(ipiv);
    assert(!A.isnull());
    assert(!B.isnull());
    assert(B.cnt_unsafe() == 1);
    assert(A.nr_unsafe() == A.nc_unsafe());
    assert(A.nr_unsafe() == B.nr_unsafe());

    // Apply row interchanges to the right hand sides (vector/matrix B).
    matrix_helper_laswp(A.nr_unsafe(), B.nc_unsafe(), const_cast<T*>(B.blas_ref_ma()), 0, A.nr_unsafe()-1, ipiv);

    // Solve L*X = B, overwriting B with X.
    matrix_helper_trsm(false, false, A.nr_unsafe(), B.nc_unsafe(), A.blas_ref_ma(), A.nr_unsafe(), const_cast<T*>(B.blas_ref_ma()));

    // Solve U*X = B, overwriting B with X.
    matrix_helper_trsm(true, true, A.nr_unsafe(), B.nc_unsafe(), A.blas_ref_ma(), A.nr_unsafe(), const_cast<T*>(B.blas_ref_ma()));

    // All ok.
    return 0;
}


// LU combined matrix as returned by LAPACK GETRF.

template <typename T>
inline void LU_dec(const MatrixBase<T> & A, MatrixBase<T> & Y)
{
    MATRIXBASE_ASSERT(!A.isnull(), "A,Y", "null matrix");

    // Special action on matrix 1x1.
    if (A.nr_unsafe() == 1 && A.nc_unsafe() == 1) {
        Y = A;
        return;
    }

    // Get a detached copy of the matrix A (will be modified).
    MatrixBase<T> A_tmp = A.template casted_copy<T>();

    // Allocate the pivot vector for the "LU decomposition"
    // (automatically deallocated when it goes out of scope).
    // We can omit the initialization of the pivot here (it
    // is not used in this function after the "getrf" call).
    std::unique_ptr<int[]> ipiv(new int[A_tmp.nr_unsafe()]);

    // Call the LU decomposition routine.
    int retval = matrix_helper_getrf(A_tmp, ipiv.get());

    // Check the result value.
    assert(retval >= 0);

    // Return the result of the LAPACK GETRF routine.
    Y = std::move(A_tmp);
}


// Return the L,U decomposition (permuted forms of the upper
// (U) and lower (L) triangular matrices, such that  A = L*U).

template <typename T>
inline void LU_dec(const MatrixBase<T> & A, MatrixBase<T> & L, MatrixBase<T> & U)
{
    MATRIXBASE_ASSERT(!A.isnull(), "A,L,U", "null matrix");

    // Special action on matrix 1x1.
    if (A.nr_unsafe() == 1 && A.nc_unsafe() == 1) {
        U = A;
        L = MatrixBase<T>(1);
        return;
    }

    // Get a detached copy of the matrix A (will be modified).
    MatrixBase<T> A_tmp = A.template casted_copy<T>();

    // Get the size of the detached matrix.
    int m = A_tmp.nr_unsafe();
    int n = A_tmp.nc_unsafe();

    // Allocate the pivot vector for the "LU decomposition"
    // (automatically deallocated when it goes out of scope).
    // Here the pivot must be initialized with FORTRAN INDEXES
    // (some indexes may be left unchanged by the lapack routine).
    std::unique_ptr<int[]> ipiv(new int[m]);
    for (int i = 0; i < m; ++i) ipiv[i] = i+1;

    // Call the LU decomposition routine.
    int retval = matrix_helper_getrf(A_tmp, ipiv.get());

    // Check the result value.
    assert(retval >= 0);

    // Prepare the output matrices.
    MatrixBase<T> L_tmp(m, std::min(m,n), true);
    MatrixBase<T> U_tmp(std::min(m,n), n, false);

    // Set the permutation vector. Note that ipiv has elements
    // in range 1..m, while idx has elements in range 0..m-1.
    std::unique_ptr<int[]> idx(new int[m]);
    for (int i = 0; i < m; ++i) idx[i] = i;
    for (int i = 0; i < m; ++i)
    {
        int k = idx[ipiv[i] - 1];
        idx[ipiv[i] - 1] = idx[i];
        idx[i] = k;
    }

    // Set the L output matrix.
    for (int j = 0; j < std::min(m,n); ++j)
        for (int i = 0; i < m; ++i)
            if (i > j)
                L_tmp.set_unsafe(idx[i],j, A_tmp.get_unsafe(i,j));
            else if (i == j)
                L_tmp.set_unsafe(idx[i],j, 1);
            else
                L_tmp.set_unsafe(idx[i],j, 0);

    // Set the U output matrix.
    for (int j = 0; j < n; ++j)
        for (int i = 0; i < std::min(m,n); ++i)
            if (i > j)
                U_tmp.set_unsafe(i,j, 0);
            else
                U_tmp.set_unsafe(i,j, A_tmp.get_unsafe(i,j));

    // Return the matrices L,U.
    L = std::move(L_tmp);
    U = std::move(U_tmp);
}


// Return the L,U,P decomposition (permuted forms of the upper
// (U) and lower (L) triangular matrices, such that P*A = L*U).

template <typename T>
inline void LU_dec(const MatrixBase<T> & A, MatrixBase<T> & L, MatrixBase<T> & U, MatrixBase<int> & P)
{
    MATRIXBASE_ASSERT(!A.isnull(), "A,L,U,P", "null matrix");

    // Special action on matrix 1x1.
    if (A.nr_unsafe() == 1 && A.nc_unsafe() == 1) {
        U = A;
        L = MatrixBase<T>(1);
        P = MatrixBase<int>(1);
        return;
    }

    // Get a detached copy of the matrix A (will be modified).
    MatrixBase<T> A_tmp = A.template casted_copy<T>();

    // Get the size of the detached matrix.
    int m = A_tmp.nr_unsafe();
    int n = A_tmp.nc_unsafe();

    // Allocate the pivot vector for the "LU decomposition"
    // (automatically deallocated when it goes out of scope).
    // Here the pivot must be initialized with FORTRAN INDEXES
    // (some indexes may be left unchanged by the lapack routine).
    std::unique_ptr<int[]> ipiv(new int[m]);
    for (int i = 0; i < m; ++i) ipiv[i] = i+1;

    // Call the LU decomposition routine.
    int retval = matrix_helper_getrf(A_tmp, ipiv.get());

    // Check the result value.
    assert(retval >= 0);

    // Prepare the output matrices.
    MatrixBase<T> L_tmp(m, std::min(m,n), false);
    MatrixBase<T> U_tmp(std::min(m,n), n, false);
    MatrixBase<int> P_tmp(m, m, true);

    // Set the permutation matrix. Note that ipiv has elements
    // in range 1..m, while idx has elements in range 0..m-1.
    std::unique_ptr<int[]> idx(new int[m]);
    for (int i = 0; i < m; ++i) idx[i] = i;
    for (int i = 0; i < m; ++i)
    {
        int k = idx[ipiv[i] - 1];
        idx[ipiv[i] - 1] = idx[i];
        idx[i] = k;
        P_tmp.set_unsafe(i, k, 1);
    }

    // Set the L output matrix.
    for (int j = 0; j < std::min(m,n); ++j)
        for (int i = 0; i < m; ++i)
            if (i > j)
                L_tmp.set_unsafe(i,j, A_tmp.get_unsafe(i,j));
            else if (i == j)
                L_tmp.set_unsafe(i,j, 1);
            else
                L_tmp.set_unsafe(i,j, 0);

    // Set the U output matrix.
    for (int j = 0; j < n; ++j)
        for (int i = 0; i < std::min(m,n); ++i)
            if (i > j)
                U_tmp.set_unsafe(i,j, 0);
            else
                U_tmp.set_unsafe(i,j, A_tmp.get_unsafe(i,j));

    // Return the matrices L,U,P.
    L = std::move(L_tmp);
    U = std::move(U_tmp);
    P = std::move(P_tmp);
}


// LEFT DIVISION A\B: conceptually equivalent to the expression
// inv(A) * B  but it is computed without forming the inverse of A:
// X = A\B is the solution to the equation AX = B computed by Gaussian
// elimination with partial pivoting (see LU for details). Matrices A
// must be square; also A and B must have the same number of rows.

template <typename S, typename T>
inline auto mldivide(const MatrixBase<S> & A, const MatrixBase<T> & B) -> MatrixBase<decltype(S()+T())>
{
    int retval;

    MATRIX_BINARY_OPMM_ASSERT(!A.isnull() && !B.isnull(), "null matrix");
    MATRIX_BINARY_OPMM_ASSERT(A.nr_unsafe() == A.nc_unsafe(), "not square matrix");     // NOTE: A must be square.
    MATRIX_BINARY_OPMM_ASSERT(A.nr_unsafe() == B.nr_unsafe(), "invalid sizes");         // NOTE: same n.of rows.

    // Get a detached copy of the matrix A (will be modified).
    MatrixBase<S> A_tmp = A.template casted_copy<S>();

    // Allocate the pivot vector for the "LU decomposition"
    // (automatically deallocated when it goes out of scope).
    // Here the pivot must be initialized with FORTRAN INDEXES
    // (some indexes may be left unchanged by the lapack routine).
    std::unique_ptr<int[]> ipiv(new int[A_tmp.nr_unsafe()]);
    for (int i = 0; i < A_tmp.nr_unsafe(); ++i) ipiv[i] = i+1;

    // Call the LU decomposition routine.
    retval = matrix_helper_getrf(A_tmp, ipiv.get());

    // Check the result value.
    assert(retval >= 0);
    MATRIX_BINARY_OPMM_ASSERT(retval == 0, "matrix is singular");

    // Get a detached copy of the vector/matrix B (will be modified).
    MatrixBase<decltype(S()+T())> B_tmp = B.template casted_copy<decltype(S()+T())>();

    // Calculate the solution of AX = B.
    retval = matrix_helper_getrs(A_tmp, B_tmp, ipiv.get());
    assert(retval == 0);

    // Return the solution.
    return B_tmp;
}


// Solve the linear system A*X = B. This function is equivalent
// to the the matrix-left-divide function  X = mldivide(A, B).

template <typename S, typename T>
inline auto linsolve(const MatrixBase<S> & A, const MatrixBase<T> & B) -> MatrixBase<decltype(S()+T())>
{
    return mldivide(A, B);
}


// RIGHT DIVISION A/B: Conceptually equivalent to the expression
// A * inv(B) but it is computed without forming the inverse of B.
// A/B is the solution of X*B = A and is computed as X = (B'\A')'.

template <typename S, typename T>
inline auto mrdivide(const MatrixBase<S> & A, const MatrixBase<T> & B) -> MatrixBase<decltype(S()+T())>
{
    int retval;

    MATRIX_BINARY_OPMM_ASSERT(!A.isnull() && !B.isnull(), "null matrix");
    MATRIX_BINARY_OPMM_ASSERT(B.nr_unsafe() == B.nc_unsafe(), "not square matrix");     // NOTE: B must be square.
    MATRIX_BINARY_OPMM_ASSERT(A.nc_unsafe() == B.nc_unsafe(), "invalid sizes");         // NOTE: same n.of columns.

    // Get the transpose of the matrix B (will be modified).
    MatrixBase<T> B_tmp = B.tran();

    // Allocate the pivot vector for the "LU decomposition"
    // (automatically deallocated when it goes out of scope).
    // Here the pivot must be initialized with FORTRAN INDEXES
    // (some indexes may be left unchanged by the lapack routine).
    std::unique_ptr<int[]> ipiv(new int[B_tmp.nr_unsafe()]);
    for (int i = 0; i < B_tmp.nr_unsafe(); ++i) ipiv[i] = i+1;

    // Call the LU decomposition routine.
    retval = matrix_helper_getrf(B_tmp, ipiv.get());

    // Check the result value.
    assert(retval >= 0);
    MATRIX_BINARY_OPMM_ASSERT(retval == 0, "matrix is singular");

    // Get the transpose of the vector/matrix A (will be modified).
    MatrixBase<decltype(S()+T())> A_tmp = A.template casted_copy<decltype(S()+T())>();
    A_tmp = A_tmp.tran();

    // Calculate the solution of B'X=A'.
    retval = matrix_helper_getrs(B_tmp, A_tmp, ipiv.get());
    assert(retval == 0);

    // Return the solution X'.
    return A_tmp.tran();
}


// Right division A/B operator, see above.

template <typename S, typename T>
inline auto operator/(const MatrixBase<S> & A, const MatrixBase<T> & B) -> MatrixBase<decltype(S()+T())>
{
    return mrdivide(A, B);
}


// Right division v/M, equivalent to v*I/M, where I is the identity matrix.

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value || std::is_complex<S>::value>>
inline auto operator/(const S v, const MatrixBase<T> & M) -> MatrixBase<decltype(S()+T())>
{
    MatrixBase<S> vI;

    if (!M.isnull())
        vI = v * MatrixBase<S>::eye(M.nr_unsafe());
    return mrdivide(vI, M);
}


// Matrix inverse.
// It is rare that one actually requires a matrix inverse. Solving A*X = B for matrix
// A and vector B is typically 2 - 3 times faster than inverting A and then multiplying
// inv(A) * B  (see https://www.johndcook.com/blog/2010/01/19/dont-invert-that-matrix/).
// Here we compute the inverse of M as inv(M) = M\I, where I is the identity matrix.

template <typename T>
inline MatrixBase<T> inv(const MatrixBase<T> & M)
{
    int retval;

    MATRIXBASE_ASSERT(!M.isnull(), "M", "null matrix");
    MATRIXBASE_ASSERT(M.nr_unsafe() == M.nc_unsafe(), "M", "not square matrix");

    // Special action on matrix 1x1.
    if (M.nr_unsafe() == 1)
        return array_div(1, M);

    // Get a detached copy of the matrix A (will be modified).
    MatrixBase<T> A_tmp = M.template casted_copy<T>();

    // Allocate the pivot vector for the "LU decomposition"
    // (automatically deallocated when it goes out of scope).
    // Here the pivot must be initialized with FORTRAN INDEXES
    // (some indexes may be left unchanged by the lapack routine).
    std::unique_ptr<int[]> ipiv(new int[A_tmp.nr_unsafe()]);
    for (int i = 0; i < A_tmp.nr_unsafe(); ++i) ipiv[i] = i+1;

    // Call the LU decomposition routine.
    retval = matrix_helper_getrf(A_tmp, ipiv.get());

    // Check the result value.
    assert(retval >= 0);
    MATRIXBASE_ASSERT(retval == 0, "M", "matrix is singular");

    // Calculate the inverse as solution of AX=B where B is the identity matrix.
    MatrixBase<T> B_tmp = MatrixBase<T>::eye(A_tmp.nr_unsafe());
    retval = matrix_helper_getrs(A_tmp, B_tmp, ipiv.get());
    assert(retval == 0);

    // Return the inverse.
    return B_tmp;
}


// Matrix determinant.

template <typename T>
inline T det(const MatrixBase<T> & M)
{
    int retval;

    MATRIXBASE_ASSERT(!M.isnull(), "M", "null matrix");
    MATRIXBASE_ASSERT(M.nr_unsafe() == M.nc_unsafe(), "M", "not square matrix");

    // Special action on matrices 1x1 and 2x2.
    if (M.nr_unsafe() == 1)
        return M.get_unsafe(0,0);
    if (M.nr_unsafe() == 2)
        return M.get_unsafe(0,0) * M.get_unsafe(1,1) - M.get_unsafe(1,0) * M.get_unsafe(0,1);

    // Get a detached copy of the matrix A (will be modified).
    MatrixBase<T> A_tmp = M.template casted_copy<T>();

    // Allocate the pivot vector for the "LU decomposition"
    // (automatically deallocated when it goes out of scope).
    // Here the pivot must be initialized with FORTRAN INDEXES
    // (some indexes may be left unchanged by the lapack routine).
    std::unique_ptr<int[]> ipiv(new int[A_tmp.nr_unsafe()]);
    for (int i = 0; i < A_tmp.nr_unsafe(); ++i) ipiv[i] = i+1;

    // Call the LU decomposition routine.
    retval = matrix_helper_getrf(A_tmp, ipiv.get());

    // Check the result value.
    assert(retval >= 0);

    // The determinat is given by product of diagonal elements on the resuling LU
    // matrix, inverted in sign by the number of transpositions in the ipiv vector.
    T d = 1;
    for(int i = 0; i < A_tmp.nr_unsafe(); ++i)
    {
        d *= A_tmp.get_unsafe(i,i);
        if (i+1 != ipiv[i])
            d = -d;
    }

    // Return the determinant.
    return d;
}



// ----------------------------- Row & column vectors ----------------------------- //

template <typename T>
class RowVecBase : public MatrixBase<T>
{
public:
    inline RowVecBase() noexcept;
    inline RowVecBase(const int nelem, const bool init = true);
    inline RowVecBase(const std::initializer_list<T> & list);
    inline RowVecBase(const int nelem, const T * array);
    inline RowVecBase(const RowVecBase<T> & M);
    inline RowVecBase(RowVecBase<T> && M);
    inline RowVecBase(const MatrixBase<T> & M);
    inline RowVecBase(MatrixBase<T> && M);
    inline RowVecBase<T> & operator=(const RowVecBase<T> & M);
    inline RowVecBase<T> & operator=(RowVecBase<T> && M) noexcept;
    inline RowVecBase<T> & operator=(const MatrixBase<T> & M);
    inline RowVecBase<T> & operator=(MatrixBase<T> && M);

    inline MatrixBaseRef<T> operator[](const int k);
    inline MatrixBaseRef<T> operator()(const int i, const int j) = delete;
    inline T operator[](const int k) const;
    inline T operator()(const int i, const int j) const = delete;
    inline RowVecBase<T> subvec(int c1, int c2) const;
    inline MatrixBase<T> submat(int r1, int c1, int r2, int c2) const = delete;
    inline void resize(const int nelem, const bool init = true);
    inline void resize(const int nrows, const int ncols, const bool init) = delete;

    static inline MatrixBase<T> eye(const int n) = delete;
    static inline RowVecBase<T> ones(const int n);
    static inline MatrixBase<T> ones(const int n, const int m) = delete;
    static inline RowVecBase<T> zeros(const int n);
    static inline MatrixBase<T> zeros(const int n, const int m) = delete;
    static inline RowVecBase<T> rand(const int n, const T lower = 0, const T upper = 1);
    static inline MatrixBase<T> rand(const int n, const int m, const T lower = 0, const T upper = 1) = delete;
    static inline RowVecBase<T> randn(const int n, const T mean = 0, const T devstd = 1);
    static inline MatrixBase<T> randn(const int n, const int m, const T mean = 0, const T devstd = 1) = delete;
    static inline MatrixBase<T> hilb(const int n) = delete;
};


template <typename T>
class ColVecBase : public MatrixBase<T>
{
public:
    inline ColVecBase() noexcept;
    inline ColVecBase(const int nelem, const bool init = true);
    inline ColVecBase(const std::initializer_list<T> & list);
    inline ColVecBase(const int nelem, const T * array);
    inline ColVecBase(const ColVecBase<T> & M);
    inline ColVecBase(ColVecBase<T> && M);
    inline ColVecBase(const MatrixBase<T> & M);
    inline ColVecBase(MatrixBase<T> && M);
    inline ColVecBase<T> & operator=(const ColVecBase<T> & M);
    inline ColVecBase<T> & operator=(ColVecBase<T> && M) noexcept;
    inline ColVecBase<T> & operator=(const MatrixBase<T> & M);
    inline ColVecBase<T> & operator=(MatrixBase<T> && M);

    inline MatrixBaseRef<T> operator[](const int k);
    inline MatrixBaseRef<T> operator()(const int i, const int j) = delete;
    inline T operator[](const int k) const;
    inline T operator()(const int i, const int j) const = delete;
    inline ColVecBase<T> subvec(int r1, int r2) const;
    inline MatrixBase<T> submat(int r1, int c1, int r2, int c2) const = delete;
    inline void resize(const int nelem, const bool init = true);
    inline void resize(const int nrows, const int ncols, const bool init) = delete;

    static inline MatrixBase<T> eye(const int n) = delete;
    static inline ColVecBase<T> ones(const int n);
    static inline MatrixBase<T> ones(const int n, const int m) = delete;
    static inline ColVecBase<T> zeros(const int n);
    static inline MatrixBase<T> zeros(const int n, const int m) = delete;
    static inline ColVecBase<T> rand(const int n, const T lower = 0, const T upper = 1);
    static inline MatrixBase<T> rand(const int n, const int m, const T lower = 0, const T upper = 1) = delete;
    static inline ColVecBase<T> randn(const int n, const T mean = 0, const T devstd = 1);
    static inline MatrixBase<T> randn(const int n, const int m, const T mean = 0, const T devstd = 1) = delete;
    static inline MatrixBase<T> hilb(const int n) = delete;
};


template <typename T>
inline RowVecBase<T>::RowVecBase() noexcept  : MatrixBase<T>()
{
}


template <typename T>
inline ColVecBase<T>::ColVecBase() noexcept  : MatrixBase<T>()
{
}


template <typename T>
inline RowVecBase<T>::RowVecBase(const int nelem, const bool init) : MatrixBase<T>(1,nelem,init)
{
}


template <typename T>
inline ColVecBase<T>::ColVecBase(const int nelem, const bool init) : MatrixBase<T>(nelem,1,init)
{
}


template <typename T>
inline RowVecBase<T>::RowVecBase(const std::initializer_list<T> & list) : MatrixBase<T>(1,list.size(),list)
{
}


template <typename T>
inline ColVecBase<T>::ColVecBase(const std::initializer_list<T> & list) : MatrixBase<T>(list.size(),1,list)
{
}


template <typename T>
inline RowVecBase<T>::RowVecBase(const int nelem, const T * array) : MatrixBase<T>(1,nelem,array)
{
}


template <typename T>
inline ColVecBase<T>::ColVecBase(const int nelem, const T * array) : MatrixBase<T>(nelem,1,array)
{
}


template <typename T>
inline RowVecBase<T>::RowVecBase(const RowVecBase<T> & M) : MatrixBase<T>(M)
{
}


template <typename T>
inline ColVecBase<T>::ColVecBase(const ColVecBase<T> & M) : MatrixBase<T>(M)
{
}


template <typename T>
inline RowVecBase<T>::RowVecBase(RowVecBase<T> && M) : MatrixBase<T>(std::move(M))
{
}


template <typename T>
inline ColVecBase<T>::ColVecBase(ColVecBase<T> && M) : MatrixBase<T>(std::move(M))
{
}


template <typename T>
inline RowVecBase<T>::RowVecBase(const MatrixBase<T> & M) : MatrixBase<T>(M)
{
    // Check copied matrix.
    MATRIXBASE_ASSERT(!this->m_d || this->m_d->nr() == 1, "M", "cannot convert to row vector");
}


template <typename T>
inline ColVecBase<T>::ColVecBase(const MatrixBase<T> & M) : MatrixBase<T>(M)
{
    // Check copied matrix.
    MATRIXBASE_ASSERT(!this->m_d || this->m_d->nc() == 1, "M", "cannot convert to column vector");
}


template <typename T>
inline RowVecBase<T>::RowVecBase(MatrixBase<T> && M) : MatrixBase<T>(std::move(M))
{
    // Check moved matrix.
    MATRIXBASE_ASSERT(!this->m_d || this->m_d->nr() == 1, "M", "cannot convert to row vector");
}


template <typename T>
inline ColVecBase<T>::ColVecBase(MatrixBase<T> && M) : MatrixBase<T>(std::move(M))
{
    // Check moved matrix.
    MATRIXBASE_ASSERT(!this->m_d || this->m_d->nc() == 1, "M", "cannot convert to column vector");
}


template<typename T>
inline RowVecBase<T> & RowVecBase<T>::operator=(const RowVecBase<T> & M)
{
    MatrixBase<T>::operator=(M);
    return *this;
}


template<typename T>
inline ColVecBase<T> & ColVecBase<T>::operator=(const ColVecBase<T> & M)
{
    MatrixBase<T>::operator=(M);
    return *this;
}


template<typename T>
inline RowVecBase<T> & RowVecBase<T>::operator=(RowVecBase<T> && M) noexcept
{
    MatrixBase<T>::operator=(std::move(M));
    return *this;
}


template<typename T>
inline ColVecBase<T> & ColVecBase<T>::operator=(ColVecBase<T> && M) noexcept
{
    MatrixBase<T>::operator=(std::move(M));
    return *this;
}


template<typename T>
inline RowVecBase<T> & RowVecBase<T>::operator=(const MatrixBase<T> & M)
{
    // First assign...
    MatrixBase<T>::operator=(M);

    // ...then check assigned matrix.
    MATRIXBASE_ASSERT(!this->m_d || this->m_d->nr() == 1, "M", "cannot convert to row vector");

    return *this;
}


template<typename T>
inline ColVecBase<T> & ColVecBase<T>::operator=(const MatrixBase<T> & M)
{
    // First assign...
    MatrixBase<T>::operator=(M);

    // ...then check assigned matrix.
    MATRIXBASE_ASSERT(!this->m_d || this->m_d->nc() == 1, "M", "cannot convert to column vector");

    return *this;
}


template<typename T>
inline RowVecBase<T> & RowVecBase<T>::operator=(MatrixBase<T> && M)
{
    // First assign...
    MatrixBase<T>::operator=(std::move(M));

    // ...then check assigned matrix.
    MATRIXBASE_ASSERT(!this->m_d || this->m_d->nr() == 1, "M", "cannot convert to row vector");

    return *this;
}


template<typename T>
inline ColVecBase<T> & ColVecBase<T>::operator=(MatrixBase<T> && M)
{
    // First assign...
    MatrixBase<T>::operator=(std::move(M));

    // ...then check assigned matrix.
    MATRIXBASE_ASSERT(!this->m_d || this->m_d->nc() == 1, "M", "cannot convert to column vector");

    return *this;
}


template <typename T>
inline MatrixBaseRef<T> RowVecBase<T>::operator[](const int k)
{
    return MatrixBase<T>::operator()(0,k);
}


template <typename T>
inline MatrixBaseRef<T> ColVecBase<T>::operator[](const int k)
{
    return MatrixBase<T>::operator()(k,0);
}


template <typename T>
inline T RowVecBase<T>::operator[](const int k) const
{
    return MatrixBase<T>::operator()(0,k);
}


template <typename T>
inline T ColVecBase<T>::operator[](const int k) const
{
    return MatrixBase<T>::operator()(k,0);
}


template <typename T>
inline RowVecBase<T> RowVecBase<T>::subvec(int c1, int c2) const
{
    return MatrixBase<T>::submat(0, c1, 0, c2);
}


template <typename T>
inline ColVecBase<T> ColVecBase<T>::subvec(int r1, int r2) const
{
    return MatrixBase<T>::submat(r1, 0, r2, 0);
}


template <typename T>
inline void RowVecBase<T>::resize(const int nelem, const bool init)
{
    MatrixBase<T>::resize(nelem ? 1 : 0, nelem ? nelem : 0, init);
}


template <typename T>
inline void ColVecBase<T>::resize(const int nelem, const bool init)
{
    MatrixBase<T>::resize(nelem ? nelem : 0, nelem ? 1 : 0, init);
}


template <typename T>
inline RowVecBase<T> RowVecBase<T>::ones(const int n)
{
    return MatrixBase<T>::ones(1, n);
}


template <typename T>
inline ColVecBase<T> ColVecBase<T>::ones(const int n)
{
    return MatrixBase<T>::ones(n, 1);
}


template <typename T>
inline RowVecBase<T> RowVecBase<T>::zeros(const int n)
{
    return MatrixBase<T>::zeros(1, n);
}


template <typename T>
inline ColVecBase<T> ColVecBase<T>::zeros(const int n)
{
    return MatrixBase<T>::zeros(n, 1);
}


template <typename T>
inline RowVecBase<T> RowVecBase<T>::rand(const int n, const T lower, const T upper)
{
    return MatrixBase<T>::rand(1, n, lower, upper);
}


template <typename T>
inline ColVecBase<T> ColVecBase<T>::rand(const int n, const T lower, const T upper)
{
    return MatrixBase<T>::rand(n, 1, lower, upper);
}


template <typename T>
inline RowVecBase<T> RowVecBase<T>::randn(const int n, const T mean, const T devstd)
{
    return MatrixBase<T>::randn(1, n, mean, devstd);
}


template <typename T>
inline ColVecBase<T> ColVecBase<T>::randn(const int n, const T mean, const T devstd)
{
    return MatrixBase<T>::randn(n, 1, mean, devstd);
}



// -------------------------------- qDebug() output ------------------------------- //

#ifdef QT_VERSION
#include <QDebug>

template <typename T>
QDebug operator<<(QDebug dbg, const Matrixbase<T> & M)
{
    std::stringstream str_out;
    M.print(str_out, "");
    dbg.noquote() << QString::fromStdString(str_out.str());
    return dbg;
}
#endif



// ------------------------ BLAS/LAPACK OPTIMIZED FUNCTIONS ----------------------- //
// BLAS/LAPACK support only types float, double, complex<float> and complex<double> //

#ifdef NOBLASLAPACK
#warning "BLAS/LAPACK optimizations disabled"
#else

// FLOAT
#define LOCAL_DATA_TYPE              float
#define LOCAL_GEMM_CALL              sgemm_
#define LOCAL_GETRF_CALL             sgetrf_
#define LOCAL_GETRS_CALL             sgetrs_
#include "matrix_blas.hxx"

// DOUBLE
#define LOCAL_DATA_TYPE              double
#define LOCAL_GEMM_CALL              dgemm_
#define LOCAL_GETRF_CALL             dgetrf_
#define LOCAL_GETRS_CALL             dgetrs_
#include "matrix_blas.hxx"

// COMPLEX<FLOAT>
#define LOCAL_DATA_TYPE              cplxf
#define LOCAL_GEMM_CALL              cgemm_
#define LOCAL_GETRF_CALL             cgetrf_
#define LOCAL_GETRS_CALL             cgetrs_
#include "matrix_blas.hxx"

// COMPLEX<DOUBLE>
#define LOCAL_DATA_TYPE              cplxd
#define LOCAL_GEMM_CALL              zgemm_
#define LOCAL_GETRF_CALL             zgetrf_
#define LOCAL_GETRS_CALL             zgetrs_
#include "matrix_blas.hxx"

#endif  // NOBLASLAPACK



// -------------------------------- USEFUL TYPEDEFS ------------------------------- //

typedef long double ldouble;

// INT
typedef MatrixBase<int>     Matrix_I;
typedef RowVecBase<int>     RowVec_I;
typedef ColVecBase<int>     ColVec_I;

// FLOAT
typedef MatrixBase<float>   Matrix_F;
typedef RowVecBase<float>   RowVec_F;
typedef ColVecBase<float>   ColVec_F;

// DOUBLE
typedef MatrixBase<double>  Matrix_D;
typedef RowVecBase<double>  RowVec_D;
typedef ColVecBase<double>  ColVec_D;

// LONG DOUBLE
typedef MatrixBase<ldouble> Matrix_LD;
typedef RowVecBase<ldouble> RowVec_LD;
typedef ColVecBase<ldouble> ColVec_LD;

// COMPLEX<INT>
typedef MatrixBase<cplxi>   Matrix_CI;
typedef RowVecBase<cplxi>   RowVec_CI;
typedef ColVecBase<cplxi>   ColVec_CI;

// COMPLEX<FLOAT>
typedef MatrixBase<cplxf>   Matrix_CF;
typedef RowVecBase<cplxf>   RowVec_CF;
typedef ColVecBase<cplxf>   ColVec_CF;

// COMPLEX<DOUBLE>
typedef MatrixBase<cplxd>   Matrix_CD;
typedef RowVecBase<cplxd>   RowVec_CD;
typedef ColVecBase<cplxd>   ColVec_CD;

// COMPLEX<LONG DOUBLE>
typedef MatrixBase<cplxld>  Matrix_CLD;
typedef RowVecBase<cplxld>  RowVec_CLD;
typedef ColVecBase<cplxld>  ColVec_CLD;

#endif // MATRIX_HXX
