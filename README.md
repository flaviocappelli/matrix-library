
## Simple matrix template class in C++14

This class has essentially educational purposes because it implements only few
operations on matrices (2D) and vectors, but it is fast and efficient almost as
much as other well known calculation packages. Its performances are due to the
use of the "reference counting" and "copy-on-write" techniques, that minimize
copy operations. Furthermore, when the template class is instantiated on float,
double, complex&lt;float&gt; and complex&lt;double&gt; types, some operations
can be performed using the optimized BLAS and LAPACK routines (currently the
matrix product, the LU decomposition, the resolution of linear systems AX=B,
the right and left division between matrices and the matrix inversion). To
take advantage of BLAS and LAPACK you must install an optimized implementation
(for example OpenBLAS + reference LAPACK). When BLAS and LAPACK are not available
(if they are not installed or because the instantiated type is not supported)
some fallback functions are used. Note that the class is easily extensible.

Why did I write this class? Mainly for fun because linear algebra fascinates
me, but also to experiment a little with BLAS/LAPACK libraries and with some
C++14 features and other programming techniques, like:

 - rvalue references, move semantics, reference qualifiers
 - specialization of template members and functions
 - SFINAE ("Substitution Failure Is Not An Error")
 - initialization lists and decltype keyword
 - implicit sharing and copy-on-write
 - proxy design pattern

For more technical details see the matrix.hxx header.

These are a couple of benchmarks on my desktop PC, that compare this class
with other packages for linear algebra. Tests are performed on matrices of
double type with increasing size (generated randomly and stored on files
at the beginning, so all packages are tested against the same data).

![alt text](img/benchmark_mm.png)

![alt text](img/benchmark_lu.png)
