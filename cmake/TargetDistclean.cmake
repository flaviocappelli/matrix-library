
add_custom_target(distclean @echo "Cleaning cmake files")

add_custom_command(
    COMMAND "${CMAKE_MAKE_PROGRAM}" clean
    COMMAND cmake -E remove_directory "${CMAKE_BINARY_DIR}/CMakeFiles"

    COMMAND cmake -E remove -f "${CMAKE_BINARY_DIR}/CMakeCache.txt"
    COMMAND cmake -E remove -f "${CMAKE_BINARY_DIR}/Makefile"
    COMMAND cmake -E remove -f "${CMAKE_BINARY_DIR}/cmake_install.cmake"

    TARGET  distclean
    VERBATIM
)
