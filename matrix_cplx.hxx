/*
** Copyright (C) 2018 Flavio Cappelli
** See Copyright Notice in matrix.hxx
*/

#ifndef MATRIX_CPLX_HXX
#define MATRIX_CPLX_HXX

#include <cmath>
#include <complex>



// Typedefs for complex types.

typedef std::complex<int>           cplxi;
typedef std::complex<float>         cplxf;
typedef std::complex<double>        cplxd;
typedef std::complex<long double>   cplxld;



// Overload of the mathematical operator '+' on pairs of non-homogeneous types
// (not provided by the standard class std::complex). C++14 "SFINAE" is used to
// instantiate the template only on primitive types (to avoid ambiguity with the
// overload of the same operator that operates on <Matrix,val> or <val,Matrix>).

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value && std::is_arithmetic<T>::value>>
inline auto operator+(const std::complex<S> x, const T y) -> std::complex<decltype(S()+T())>
{
    typedef decltype(S()+T()) ret_t;
    return static_cast<std::complex<ret_t>>(x) + static_cast<ret_t>(y);
}

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value && std::is_arithmetic<T>::value>>
inline auto operator+(const S x, const std::complex<T> y) -> std::complex<decltype(S()+T())>
{
    typedef decltype(S()+T()) ret_t;
    return static_cast<ret_t>(x) + static_cast<std::complex<ret_t>>(y);
}

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value && std::is_arithmetic<T>::value>>
inline auto operator+(const std::complex<S> x, const std::complex<T> y) -> std::complex<decltype(S()+T())>
{
    typedef decltype(S()+T()) ret_t;
    return static_cast<std::complex<ret_t>>(x) + static_cast<std::complex<ret_t>>(y);
}



// Overload of the mathematical operator '-' on pairs of non-homogeneous types
// (not provided by the standard class std::complex). C++14 "SFINAE" is used to
// instantiate the template only on primitive types (to avoid ambiguity with the
// overload of the same operator that operates on <Matrix,val> or <val,Matrix>).

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value && std::is_arithmetic<T>::value>>
inline auto operator-(const std::complex<S> x, const T y) -> std::complex<decltype(S()+T())>
{
    typedef decltype(S()+T()) ret_t;
    return static_cast<std::complex<ret_t>>(x) - static_cast<ret_t>(y);
}

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value && std::is_arithmetic<T>::value>>
inline auto operator-(const S x, const std::complex<T> y) -> std::complex<decltype(S()+T())>
{
    typedef decltype(S()+T()) ret_t;
    return static_cast<ret_t>(x) - static_cast<std::complex<ret_t>>(y);
}

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value && std::is_arithmetic<T>::value>>
inline auto operator-(const std::complex<S> x, const std::complex<T> y) -> std::complex<decltype(S()+T())>
{
    typedef decltype(S()+T()) ret_t;
    return static_cast<std::complex<ret_t>>(x) - static_cast<std::complex<ret_t>>(y);
}



// Overload of the mathematical operator '*' on pairs of non-homogeneous types
// (not provided by the standard class std::complex). C++14 "SFINAE" is used to
// instantiate the template only on primitive types (to avoid ambiguity with the
// overload of the same operator that operates on <Matrix,val> or <val,Matrix>).

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value && std::is_arithmetic<T>::value>>
inline auto operator*(const std::complex<S> x, const T y) -> std::complex<decltype(S()*T())>
{
    typedef decltype(S()*T()) ret_t;
    return static_cast<std::complex<ret_t>>(x) * static_cast<ret_t>(y);
}

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value && std::is_arithmetic<T>::value>>
inline auto operator*(const S x, const std::complex<T> y) -> std::complex<decltype(S()*T())>
{
    typedef decltype(S()*T()) ret_t;
    return static_cast<ret_t>(x) * static_cast<std::complex<ret_t>>(y);
}

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value && std::is_arithmetic<T>::value>>
inline auto operator*(const std::complex<S> x, const std::complex<T> y) -> std::complex<decltype(S()*T())>
{
    typedef decltype(S()*T()) ret_t;
    return static_cast<std::complex<ret_t>>(x) * static_cast<std::complex<ret_t>>(y);
}



// Overload of the mathematical operator '/' on pairs of non-homogeneous types
// (not provided by the standard class std::complex). C++14 "SFINAE" is used to
// instantiate the template only on primitive types (to avoid ambiguity with the
// overload of the same operator that operates on <Matrix,val> or <val,Matrix>).

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value && std::is_arithmetic<T>::value>>
inline auto operator/(const std::complex<S> x, const T y) -> std::complex<decltype(S()*T())>
{
    typedef decltype(S()*T()) ret_t;
    return static_cast<std::complex<ret_t>>(x) / static_cast<ret_t>(y);
}

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value && std::is_arithmetic<T>::value>>
inline auto operator/(const S x, const std::complex<T> y) -> std::complex<decltype(S()*T())>
{
    typedef decltype(S()*T()) ret_t;
    return static_cast<ret_t>(x) / static_cast<std::complex<ret_t>>(y);
}

template <typename S, typename T, typename = std::enable_if_t<std::is_arithmetic<S>::value && std::is_arithmetic<T>::value>>
inline auto operator/(const std::complex<S> x, const std::complex<T> y) -> std::complex<decltype(S()*T())>
{
    typedef decltype(S()*T()) ret_t;
    return static_cast<std::complex<ret_t>>(x) / static_cast<std::complex<ret_t>>(y);
}



// The standard C++ library currently do not implement type traits
// for the complex floating point types. This is my implementation.

namespace std
{
    // is_complex_integral

    template <typename T>
    struct is_complex_integral {
      static const bool value = false;
    };

    template <>
    struct is_complex_integral<std::complex<bool>> {
      static const bool value = true;
    };

    template <>
    struct is_complex_integral<std::complex<char>> {
      static const bool value = true;
    };

    template <>
    struct is_complex_integral<std::complex<signed char>> {
      static const bool value = true;
    };

    template <>
    struct is_complex_integral<std::complex<unsigned char>> {
      static const bool value = true;
    };

    template <>
    struct is_complex_integral<std::complex<short>> {
      static const bool value = true;
    };

    template <>
    struct is_complex_integral<std::complex<unsigned short>> {
      static const bool value = true;
    };

    template <>
    struct is_complex_integral<std::complex<int>> {
      static const bool value = true;
    };

    template <>
    struct is_complex_integral<std::complex<unsigned int>> {
      static const bool value = true;
    };

    template <>
    struct is_complex_integral<std::complex<long>> {
      static const bool value = true;
    };

    template <>
    struct is_complex_integral<std::complex<unsigned long>> {
      static const bool value = true;
    };

    template <>
    struct is_complex_integral<std::complex<long long>> {
      static const bool value = true;
    };

    template <>
    struct is_complex_integral<std::complex<unsigned long long>> {
      static const bool value = true;
    };


    // is_complex_floating_point

    template <typename T>
    struct is_complex_floating_point {
      static const bool value = false;
    };

    template <>
    struct is_complex_floating_point<std::complex<float>> {
      static const bool value = true;
    };

    template <>
    struct is_complex_floating_point<std::complex<double>> {
      static const bool value = true;
    };

    template <>
    struct is_complex_floating_point<std::complex<long double>> {
      static const bool value = true;
    };


    // is_complex

    template <typename T>
    struct is_complex : std::integral_constant<bool,
            is_complex_integral<T>::value || is_complex_floating_point<T>::value> {};
}



// These permit to retrieve the base type of complex (i.e.
// int from complex<int>, float from complex<float>, etc).

template <typename C>
struct get_complex_subtype;

template <template <typename ...> class C, typename T>
struct get_complex_subtype<C<T>>
{
    using type = T;
};

#endif // MATRIX_CPLX_HXX
