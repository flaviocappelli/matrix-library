/*
** Copyright (C) 2018 Flavio Cappelli
** See Copyright Notice in matrix.hxx
*/

// WARNING - BLAS/LAPACK OPTIMIZED FUNCTIONS.
// This file is included many times within "matrix.hxx" (with macros having
// different values) so it must not be protected by #ifdef/#endif directives.

// Include "matrix.hxx" just for the inline syntax check in code editors.
#ifndef MATRIX_HXX
#include "matrix.hxx"
#endif



// //////////////////////////////////////////////////////////////////////////////// //
//                  BLAS/LAPACK interface (use Fortran API, not C)                  //
// //////////////////////////////////////////////////////////////////////////////// //

// About mixing C/C++ with Fortran code, see:
//  - http://www.math.utah.edu/software/c-with-fortran.html
//  - http://www.yolinux.com/TUTORIALS/LinuxTutorialMixingFortranAndC.html

extern "C" {
extern void LOCAL_GEMM_CALL(const char *transa, const char *transb,
                            const int *m, const int *n, const int *k,
                            const LOCAL_DATA_TYPE *alpha,
                            const LOCAL_DATA_TYPE *a, const int *lda,
                            const LOCAL_DATA_TYPE *b, const int *ldb,
                            const LOCAL_DATA_TYPE *beta,
                            LOCAL_DATA_TYPE *c, const int *ldc);

extern void LOCAL_GETRF_CALL(const int *m, const int *n, LOCAL_DATA_TYPE *a,
                             const int *lda, int *ipiv, int *info);

extern void LOCAL_GETRS_CALL(const char *trans, const int *n, const int *nrhs,
                             const LOCAL_DATA_TYPE *a, const int *lda, const int *ipiv,
                             LOCAL_DATA_TYPE *b, const int *ldb, int *info);
}



// Matrix product, optimized using BLAS GEMM on matrices of the SAME TYPE.

template <>
auto operator*(const MatrixBase<LOCAL_DATA_TYPE> & A, const MatrixBase<LOCAL_DATA_TYPE> & B) -> MatrixBase<LOCAL_DATA_TYPE>
{
    MATRIX_BINARY_OPMM_ASSERT(!A.isnull() && !B.isnull(), "null matrix");
    MATRIX_BINARY_OPMM_ASSERT(A.nc_unsafe() == B.nr_unsafe(), "invalid matrix sizes");

    MatrixBase<LOCAL_DATA_TYPE> Tmp(A.nr_unsafe(), B.nc_unsafe(), false);

    // Do not use BLAS for small matrices.
    if (A.nr_unsafe() < 5 && A.nc_unsafe() < 5 && B.nc_unsafe() < 5) {
        for(int i = 0; i < A.nr_unsafe(); ++i) {
            for (int j = 0; j < B.nc_unsafe(); ++j) {
                LOCAL_DATA_TYPE sum = 0;
                for (int k = 0; k < A.nc_unsafe(); ++k)
                    sum += A.get_unsafe(i,k) * B.get_unsafe(k,j);
                Tmp.set_unsafe(i,j, sum);
            }
        }
    } else {
        const LOCAL_DATA_TYPE alpha = 1;
        const LOCAL_DATA_TYPE beta  = 0;
        LOCAL_GEMM_CALL("N",                            // A not transposed.
                        "N",                            // B not transposed.
                        Tmp.blas_ref_nr(),
                        Tmp.blas_ref_nc(),
                        A.blas_ref_nc(),
                        &alpha,
                        A.blas_ref_ma(),
                        A.blas_ref_nr(),
                        B.blas_ref_ma(),
                        B.blas_ref_nr(),
                        &beta,
                        const_cast<LOCAL_DATA_TYPE *>(Tmp.blas_ref_ma()),
                        Tmp.blas_ref_nr()
        );
    }
    return Tmp;
}



// LU factorization helper, optimized using LAPACK GETRF. Here, the matrix
// A can be M by N. NOTE: the matrix A is modified but not detached here.

template <>
int matrix_helper_getrf(MatrixBase<LOCAL_DATA_TYPE> & A, int *ipiv)
{
    assert(ipiv);
    assert(!A.isnull());
    assert(A.cnt_unsafe() == 1);

    int info;
    LOCAL_GETRF_CALL(A.blas_ref_nr(),
                     A.blas_ref_nc(),
                     const_cast<LOCAL_DATA_TYPE *>(A.blas_ref_ma()),
                     A.blas_ref_nr(),
                     ipiv,
                     &info);
    return info;
}



// Optimized LAPACK helper that solves a system AX=B of linear equations
// using the LU factorization computed using above GETRF. Matrix A must be
// N by N. NOTE: the vector/matrix B is modified but not detached here.

template <>
int matrix_helper_getrs(const MatrixBase<LOCAL_DATA_TYPE> & A, MatrixBase<LOCAL_DATA_TYPE> & B, const int *ipiv)
{
    assert(ipiv);
    assert(!A.isnull());
    assert(!B.isnull());
    assert(B.cnt_unsafe() == 1);
    assert(A.nr_unsafe() == A.nc_unsafe());             // Required.
    assert(A.nr_unsafe() == B.nr_unsafe());

    int info;
    LOCAL_GETRS_CALL("N",                               // A not transposed.
                     A.blas_ref_nr(),
                     B.blas_ref_nc(),
                     A.blas_ref_ma(),
                     A.blas_ref_nr(),
                     ipiv,
                     const_cast<LOCAL_DATA_TYPE *>(B.blas_ref_ma()),
                     A.blas_ref_nr(),
                     &info);
    return info;
}

#undef LOCAL_DATA_TYPE
#undef LOCAL_GEMM_CALL
#undef LOCAL_GETRF_CALL
#undef LOCAL_GETRS_CALL

